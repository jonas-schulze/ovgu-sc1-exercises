#include <stdio.h>

/**
 * @brief Evaluates polynomial specified by coeffs and deg at location x
 * @param coeffs double array of coefficients
 * @param deg degree of polynomial (length of coeffs - 1)
 * @param x location where the polynomial is evaluated
 * @returns value at location x
 *
 * Naive implementation which will get numerically unstable and needs more flops than the Horner
 * scheme. 2*(deg+1) + (deg+1)*deg/2 flops per evaluation.
 */
double naive_poly_eval(double* coeffs, int deg, double x)
{
    int flop_count = 0;
    double res     = 0.0;
    // x_i            = 1.0;

    for (int i = 0; i <= deg; i++)
    {
        double x_i = 1.0;
        for (int j = 0; j < i; j++)
        {
            x_i *= x;
            ++flop_count;
        }
        res = res + coeffs[i] * x_i;
        flop_count += 2;
    }
    printf("Flops naive: %d\n", flop_count);
    return res;
}

/**
 * @brief Evaluates polynomial specified by coeffs and deg at location x
 * @param coeffs double array of coefficients
 * @param deg degree of polynomial (length of coeffs - 1)
 * @param x location where the polynomial is evaluated
 * @returns value at location x
 *
 * Implements the Horner-scheme which needs 2*(deg+1) flops for one evaluation
 */
double poly_eval(double* coeffs, int deg, double x)
{
    int flop_count = 0;
    double res     = 0.0;

    for (int i = deg; i >= 0; i--)
    {
        res = res * x + coeffs[i];
        flop_count += 2;
    }
    printf("Flops horner: %d\n", flop_count);
    return res;
}

int main()
{
    double coeffs[7] = {-998, -998, -998, -998, -998, -998, 1};
    int deg          = 6;
    double x         = 999;
    printf("Horner at x = 999: %lf\n", poly_eval(coeffs, deg, x));
    printf("Naive at x = 999: %lf\n", naive_poly_eval(coeffs, deg, x));
    return 0;
}

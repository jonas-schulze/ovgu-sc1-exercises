#include <stdio.h>
#include <math.h>

double forward_recursion(int n)
{
    double y0 = log(1.1);
    for (int i = 1; i <= n; i++)
    {
        // Compute y_i = -10y_{i-1} + 1/i
        y0 *= -10;
        y0 += 1.0 / i;
    }
    return y0;
}

double backward_recursion(int n)
{
    double y30 = 0;
    for (int i = 30; i > n; i--)
    {
        // Compute y_{i-1} = (-y_i + 1/i)/10
        y30 *= -1;
        y30 += 1.0 / i;
        y30 /= 10;
    }
    return y30;
}

int main()
{
    printf("i forward backward\n");
    double fw, bw;
    for (int i = 0; i <= 30; i++)
    {
        fw = forward_recursion(i);
        bw = backward_recursion(i);
        printf("%2d % .5e % .5e\n", i, fw, bw);
    }
}

---
title: Exercise 5
header-includes:
  - \usepackage{amsmath}
---

\newcommand{\abs}[1]{\lvert #1 \rvert}

I wasn't able to conclude any useful inequalities relating $E$ and $\tilde{E}$,
see Appendix for details.
However, if we describe $\hat{x}=x(1+\delta)$ using some $\delta$,
$\delta$ not necessarily $\approx 0$, we obtain
\begin{align*}
  \tilde{E} := \tilde{E}_{\text{rel}}(\hat{x})
  &= \frac{\abs{x-\hat{x}}}{\abs{\hat{x}}}
  = \frac{\abs{\delta}}{\abs{1+\delta}} \\
  E := E_{\text{rel}}(\hat{x})
  &= \frac{\abs{x-\hat{x}}}{\abs{x}}
  = \abs{\delta}
\end{align*}
or $E = \abs{1+\delta}\tilde{E}$
and conclude that for $\delta \not\approx 0$ these metrics may not related
at all.

Using $\tilde{E}$ does not seem to be justified.
However, if there is some other reasoning for $\delta \approx 0$ and the
algorithm is iterative and converging, it is an easy to compute approximation
of $E$, when
$$
  \abs{\hat{x}^{(i-1)}-\hat{x}^{(i)}}
  \leq \abs{x-\hat{x}^{(i)}} + \abs{x-\hat{x}^{(i-1)}}
  \leq 2\abs{x-\hat{x}^{(i-1)}}
$$
is used as the denominator.

# Appendix

I omit the proof (using triangle inequality), since this inequality does not
really allow for reasoning between the two relative errors.
$$
  \tilde{E} \geq 1-E^{-1}
$$

# Exercise 1

\def\epsilon\varepsilon
\def\dx{\textup{d}x}

## Part a

As $x^0$ is not defined for $x=0$, consider
$$
  y_0 = \lim_{\epsilon\to 0} \int_\epsilon^1 x^0 \frac{1}{10+x} \dx
$$
instead. Thus:
\begin{align*}
  y_0 
  &= \lim_{\epsilon\to 0} \int_\epsilon^1 \frac{1}{10+x} \dx \\
  &= \lim_{\epsilon\to 0} \big[ \log(10+x) \big]_\epsilon^1 \\
  &= \lim_{\epsilon\to 0} \log 11 - \log(10+\epsilon) \\
  &= \log 11 - \log 10
\end{align*}
Since the above restriction corresponds to a zero set, the original notation
remains justified. Therefore, it is $\forall n > 0$
\begin{align*}
  y_n + 10y_{n-1} - \frac{1}{n}
  &= \int_0^1 x^n \frac{1}{10+x} \dx + 10\int_0^1 x^{n-1} \frac{1}{10+x} \dx - \frac{1}{n} \\
  &= \int_0^1 (x+10) \cdot x^{n-1} \frac{1}{10+x} \dx - \frac{1}{n} \\
  &= \int_0^1 x^{n-1} \dx - \frac{1}{n} \\
  &= \bigg[ \frac{1}{n}x^n \bigg]_0^1 - \frac{1}{n} \\
  &= \frac{1}{n} - 0 - \frac{1}{n} = 0
\end{align*}
which is equivalent to the proposition.

## Part b

For $n<9$ the computed valued are almost identical.
From $n>12$ onwards the results differ even in their first digits.
This is caused by $10y_8 \approx 1/9$ (cancellation) and amplified by the
factor $-10$ in every subsequent step.

## Exercise 6
1.

\begin{equation*}
    \sqrt{1+x}-1 = \frac{(\sqrt{1+x}-1)(1+\sqrt{1+x})}{1+\sqrt{1+x}}=\frac{x}{1+\sqrt{1+x}}
\end{equation*}

2.
\begin{equation*}
    \frac{1-\cos x}{\sin x} =
    \frac{\sin^2\frac{x}{2}+\cos^2\frac{x}{2}-\left(\cos^2\frac{x}{2}-\sin^2\frac{x}{2}\right)}
    {2\sin\frac{x}{2}\cos\frac{x}{2}}
    =\frac{\sin\frac{x}{2}}{\cos\frac{x}{2}}=\tan\frac{x}{2}
\end{equation*}
    Identities used:
    * $1 = \sin^2\frac{x}{2}+\cos^2\frac{x}{2}$
    * $\cos x = \cos^2\frac{x}{2}-\sin^2\frac{x}{2}$
    * $\sin x  = 2\sin\frac{x}{2}\cos\frac{x}{2}$

3.

\begin{equation*}
    \frac{1}{1+2x}-\frac{1-x}{1+x}
    = \frac{x}{x+1}-\frac{1}{x+1}+ \frac{x}{2x+1}
    =1-\frac{2}{x+1}+\frac{1}{2x+1}
\end{equation*}

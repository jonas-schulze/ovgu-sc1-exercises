# Exercise 3

\newcommand{\e}[1]{\cdot \base^{#1}}

## a) M(10,3,-2,2)
\def\base{10}

$$ \gamma(0.5403...) = 0.540 \e{0}, -2 \leq 0 \leq 2 $$
$$ \gamma(\pi) = \gamma(3.141...) = 0.314 \e{1}, -2 \leq 1 \leq 2 $$

## c) M(2,5,-2,2)
\def\base{2}

$$
  \gamma(0.5403...)
= \gamma((
  \underbrace{0.1000101}_{=\rlap{\footnotesize 0.5390625<0.5403...}}
  ...)_2)
= (0.10001)_2 \e{0}, -2 \leq 0 \leq 2
$$
$$
  \gamma(\pi)
= \gamma(3.141...)
= \gamma((
  \underbrace{11.001001}_{=\rlap{\footnotesize 3.140625<3.141...}}
  ...)_2)
= (0.11001)_2 \e{2}, -2 \leq 2 \leq 2
$$

## b) M(2,3,-2,3)

Using the previous results:
$$
  \gamma(0.5403...)
= (0.100)_2 \e{0}, -2 \leq 0 \leq 3
$$
$$
  \gamma(\pi)
= (0.110)_2 \e{2}, -2 \leq 2 \leq 3
$$

#!/bin/sh

# Abort on errors
set -e

CURRENT_DIR=$(pwd)

# Go to the base directory
cd "$(dirname $0)"
cd ..

# Determine which tutorial to export:
#
# * take the first argument, i.e. $1
# * if not present, take the `tutorial*` folder having the highest number
TUTORIAL=${1:-$(ls -1 | grep "tutorial" | sort | tail -n 1)}
OUT_DIR=tmp/${TUTORIAL}
OUT_ARCHIVE="${OUT_DIR}.tar.bz2"

# Recreate the output directory to make sure it's empty
rm -rf "${OUT_DIR}"
rm -f "${OUT_ARCHIVE}"
mkdir -p "${OUT_DIR}"

# Tidy up first
(cd ${TUTORIAL} && make clean)

# Prepend all the C files with the group information
for file in $(ls ${TUTORIAL});
do
  if [ ${file##*.} == 'c' ];
  then
    cat <<COMMENT > "${OUT_DIR}/${file}"
// Authors:
// Jonas Schulze
// Nils Margenberg

COMMENT
  fi
  cat "${TUTORIAL}/${file}" >> "${OUT_DIR}/${file}"
done

# Compress the result
tar -cjf "${OUT_ARCHIVE}" -C "${OUT_DIR}" .
echo "Created ${OUT_ARCHIVE}"

# Go back to working directory
cd "${CURRENT_DIR}"

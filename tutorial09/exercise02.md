## Exercise 2

* Cache transfer rate: $2.13\,\text{GHz}\cdot 128\text{bit}=34.08\frac{\text{GB}}{\text{s}}$
* $\frac{2\cdot3}{1066}=177.66\,ns$ multiplication by the clock rate gives 379 needed cycles
* fetching from local storage generates even more overhead

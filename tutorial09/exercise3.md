---
title: Exercise 3
author:
- Jonas Schulze
- Nils Margenberg
---

Computer 3 shows it the clearest:

1. very flat linear part in the beginning,
2. much steeper linear part, roughly of the same "size" as the first part,
3. not-that-steep linear part, until dimension ~$0.5 * 10^6$,
    with an extrapolation also leading to the origin,
4. a-little-steeper-than-3-but-not-as-steep-as-2 linear part, from ~$0.5*10^6$ on.

Computer 1 shows the very same behavior, parts 1 and 2 are just barely visible on the plot.
Computer 4 looks somewhat similar, somewhat different, having much more noise.
It will be further discussed in the end.
Computer 2 is linear throughout.

1. probably runs on L1 cache.
2. confuses me. It should use L2 cache, but why does it have a steeper slope than part 4?
3. might be L3 cache.
4. should be RAM accesses.

For machine 3,
eyeballing leads to ~$0.5 / 2^5 * 10^6 * 3 * 8$ Byte = 375 kB of L1,
which sounds way too big, though.
On the other hand, ~$0.4 * 10^6 * 24$ byte = 9.6 MB is a reasonable size of a L3 cache.
For machine 1, these values are much (much) smaller.
Machine 2 does not seem to use any caches.

Back to machine 4.
As the absolute timings are close to the ones of computer 1,
I assume that there was another CPU-intensive task running at the same time,
especially from 0.25 to 0.7, so I wouldn't trust these measurements too much.
Or maybe it is a multi-socket machine that has the benchmark scheduled on different NUMA-nodes,
therefore causing higher measurements every once and a while.
The final kink at dimension ~$1.8*10^6$,
i.e. ~$1.8*10^6*3*8=43.2$ MB memory usage,
which would be a reasonable size for a server CPU,
supports this hypothesis.

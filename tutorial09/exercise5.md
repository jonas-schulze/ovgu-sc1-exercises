---
title: Exercise 5
author:
- Jonas Schulze
- Nils Margenberg
---

\newcommand\R{\mathbb{R}}
\newcommand\norm[1]{\lVert #1 \rVert}

Let's first fix the output to $Z := Z_j$, meaning the last value of $j \leq j_{\max}$ used.

* Obviously, it is not necessary to store $W_0, \ldots W_{j-1} \in \R^{n,m}$ and
    $Z_k \in \R^{n,mk}$ for $k < j$.
* Line 4 might run out of memory.
* $tol \cdot \norm{B^TB}$ should be computed up front.
    However, it's just one number, so it doesn't need to be moved to disk.
* Computing $A+\alpha_pI$ does only modify $n$ components, but depending on the
    sparsity of the underlying data structures and specialization of the
    algorithms used, it might be better to compute all the $n \cdot p_{\max}$
    diagonal elements (stored as $p_{\max}$ vectors) up front and copy them back
    into $A$, whenever they are needed. It might help to move these vectors to
    disk, if $p_{\max}$ is $\geq n$, but $A$ should remain in memory, especially
    as it is safe to use it as a placeholder matrix for all the $A+\alpha_p$.
* At the expense of some accuracy, it might be better to not store $Z_j$ at all.
    Moving $-2 \alpha_p V_j =: \tilde{Z}$ to disk and keeping track of the
    accosiated $p$, one is able to compute $Z = \tilde{Z} / \sqrt{2\alpha_p}$
    in the very last step. Note that $\alpha_i \in \R$.

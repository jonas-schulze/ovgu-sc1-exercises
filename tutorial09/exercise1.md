---
title: Exercise 1
author:
- Jonas Schulze
- Nils Margenberg
---

Element $A_{ij}$ is accessed for $C_{ik}$ for any $k$, i.e. $n$ times.
Similarly $B_{ij}$ is accessed for $C_{kj}$ for any $k$, i.e. also $n$ times.
$A$ is accessed row-major; assuming column-major storage, this is inefficient.
$B$ and $C$ are accessed column-major, so this is no problem.
However, $A$ is traversed much "faster" and many times more than $C$ is.
Moving the initialization of $C$ into a separate loop and changing the
inner-most command to
$$
  C_{ik} = C_{ik} + A_{ij} B_{jk}
$$
will traverse everything column-major.

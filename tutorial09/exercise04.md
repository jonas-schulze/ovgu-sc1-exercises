## Exercise 4

|                        | Clock Rate | Distance              |
|------------------------|------------|-----------------------|
| EDO-RAM                | 66         | $4.54$   |
| SD-RAM                 | 133        | $2.25$   |
| DDR-RAM                | 200        | $1.49$   |
| DDR2-RAM               | 533        | $0.559$  |
| DDR3-RAM               | 1066       | $0.2811$  |
| DDR4-RAM               | 1600       | $0.187$  |
| Intel Core i7-6700     | 3.40       | $0.088$ |
| Intel Core i7-6700     | 900        | $0.333$  |
| Intel Xeon Silver 4110 | 2.1        | $0.1427$  |
| IBM POWER8+            | 4.023      | $0.0745$  |
| Ethernet 100Mbit       | 31.25      | $10$   |
| Ethernet 10Gbit        | 417        | $0.719$ |

Since the distances are huge keeping the time passed in mind,
distance has a big influence on the design of electric circuits.
This gets even more important under the limitation mentioned in the end.

* Heat issues
* Under ideal conditions, the limiting factor becomes the longest path
a signal has to travel before a clock cycle can be finished 
# Exercise 3
## Part a

$$
(1011.101)_2
= 2^3 + 2^1 + 2^0 + 2^{-1} + 2^{-3}
= 8 + 2 + 1 + 0.5 + 0.125
= 11.625
$$

Also, as there is no number in between:
$$
(0.0111...)_2
= (0.1)_2
= 0.5
$$
Alternatively, one may use the geometric series with basis 0.5 to obtain the
same result.

## Part b

Into decimal:
$$
(1CBA)_{16}
= 1*16^3 + 12*16^2 + 11*16 + 10
= 4096 + 3072 + 176 + 10
= 7354
$$

\begin{align*}
(C2D2.E3)_{16}
&= 12*16^3 + 2*16^2 + 13*16 + 2 + 14*16^{-1} + 3*16^{-2} \\
&= 49152 + 512 + 208 + 2 + 0.875 + 0.01171875 \\
&= 49874.88671875
\end{align*}

Into binary, using Exercise 4:
$$
(1CBA)_{16}
= (1~1100~1011~1010)_2
$$

$$
(C2D2.E3)_{16}
= (1100~0010~1101~0010.1110~0011)_2
$$

## Part c

$$
(131)_{10}
= 8*16 + 3
= (83)_{16}
$$

$$
(0.3)_{10}
= \frac{4.8}{16}
= (0.4)_{16} + \frac{0.8}{16}
= (0.4)_{16} + \frac{12.8}{16^2}
= (0.4C)_{16} + \frac{0.8}{16^2}
= ...
= (0.4CCC...)_{16}
$$

## Part d

$$
(763)_{10}
= 1*8^3 + 3*8^2 + 7*8 + 3
= (1373)_8
$$

Analogously to Exercise 4, three binary digits represent one octal digit. Thus:
$$
(101~101)_2
= (55)_8
$$

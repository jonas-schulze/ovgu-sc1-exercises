## Exercise 6b
Proof that for two neighboring powers $p^b$ and $p^{b+1}$ the number of
representable elements in $\mathbb{M}$ is the same independent of the choice of b.

Obviously it holds that
<!-- \begin{align*}
\mathbb{M}(p,\:t,\:e_{\min},\:e_{\max})&=
\lbrace \pm 0.\,\alpha_1\,\alpha_2\dots\alpha_t\cdot p^b\mid
\alpha_i\in\lbrace 0,\dots,\:p-1\rbrace,\\
&\qquad\:\alpha_1\neq 0,\:
e_{\min}\leq b\leq e_{\max} \rbrace\cup\lbrace 0\rbrace\\
&=\bigcup\limits_{b=e_{\min}}^{e_{\max}}\lbrace \pm 0.\alpha_1\,\alpha_2\dots\alpha_t\cdot p^b\mid
\alpha_i\in\lbrace 0,\dots,\:p-1\rbrace,\:\alpha_1\neq 0\rbrace\\
&\qquad\cup\lbrace 0\rbrace
\end{align*} -->

\begin{multline*}
\mathbb{M}(p,\:t,\:e_{\min},\:e_{\max})=\\
\lbrace \pm 0.\,\alpha_1\,\alpha_2\dots\alpha_t\cdot p^b\mid
\alpha_i\in\lbrace 0,\dots,\:p-1\rbrace,\:\alpha_1\neq 0,\:
e_{\min}\leq b\leq e_{\max} \rbrace\cup\lbrace 0\rbrace\\
=\bigcup\limits_{b=e_{\min}}^{e_{\max}}\lbrace \pm 0.\alpha_1\,\alpha_2\dots\alpha_t\cdot p^b\mid
\alpha_i\in\lbrace 0,\dots,\:p-1\rbrace,\:\alpha_1\neq 0\rbrace
\cup\lbrace 0\rbrace
\end{multline*}

Each set of the union is of equal size, because there are
$(p-1)\cdot p^{t-1}$ sequences of $\alpha_1\,\alpha_2\dots\alpha_t$.
Thus the number of representable elements in each of these sets is
independent of b. The bigger neighboring interval of a power $b$, the interval of power $b+1$, is p times bigger.

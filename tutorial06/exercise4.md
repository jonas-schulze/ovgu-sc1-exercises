# Exercise 4

Let $x=(\alpha_{4n+k}\ldots\alpha_0)_2$, $0 \leq k \leq 3$, and $\alpha_m := 0$
for $m > 4n+k$. Thus:

\begin{align*}
x
&= \sum_{i=0}^{4n+k} \alpha_i 2^i \\
&= \sum_{j=0}^{n+1} \alpha_{4j+0} 2^{4j+0} + \alpha_{4j+1} 2^{4j+1} + \alpha_{4j+2} 2^{4j+2} + \alpha_{4j+3} 2^{4j+3} \\
&= \sum_{j=0}^{n+1} \underbrace{
  (\alpha_{4j+0} 2^0 + \alpha_{4j+1} 2^1 + \alpha_{4j+2} 2^2 + \alpha_{4j+3} 2^3)
}_{=: \beta_j} 16^j \\
\end{align*}

Observe that $\beta_j \in \{0, \ldots, 2^4-1=15\}$ since $\alpha_i \in \{0,1\}$.
Therefore, $\beta_j$ may be considered as a hexadecimal digit.
Vice versa, a hexadecimal digit $\beta_j$ may be split into four binary digits,
$(\alpha_{4j+3}\alpha_{4j+2}\alpha_{4j+1}\alpha_{4j})_2 * 16^j$.

Analogously, for the fractional part $x'=(0.\alpha_{-1}\ldots\alpha_{-4n'-k'})_2$,
we obtain:

$$
x' = \sum_{j=1}^{n'+1} \underbrace{
  (\alpha_{-4j+0} 2^0 + \alpha_{-4j+1} 2^1 + \alpha_{-4j+2} 2^2 + \alpha_{-4j+3} 2^3)
}_{=: \beta_{-j}} 16^{-j}
$$

Note that the order of the digits within a group of four has not changed
compared to the integer part.

For the general case consider $x+x'$ seperately. \hfill $\square$

## Appendix

What's the name of the "decimal point" in hex?

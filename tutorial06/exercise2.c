#include <stdio.h>
#include <fenv.h>

typedef union
{
    double d;
    long int i;
} num;

void test()
{
    num v, one;
    one.d = v.d = 1;
    v.i++;
    while (v.d * (1 / v.d) == 1 && v.d < 2)
    {
        v.i++;
    }
    if (!(v.d < 2))
    {
        printf("This is interesting.\n");
    }
    else
    {
        printf("x = %20.20lf doesn't satisfy x*(1/x) == 1.\n", v.d);
        printf("Needed %ld increment(s).\n", v.i - one.i);
    }
}

int main()
{
    printf("=== Using default rounding:\n");
    test();
    printf("=== Using FE_UPWARD:\n");
    fesetround(FE_UPWARD);
    test();
    printf("=== Using FE_DOWNWARD:\n");
    fesetround(FE_DOWNWARD);
    test();
    printf("=== Using FE_TOWARDZERO:\n");
    fesetround(FE_TOWARDZERO);
    test();
}

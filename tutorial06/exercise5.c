#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

/**
 * @brief next_variation generates the next greater lexicographic variation of a range of elements
 * @param begin pointer to the first element in the range
 * @param end pointer to the last element in the range
 * @param start minimal value for elements
 * @param upper maximal value for elements
 * @returns whether a next variation exists
 *
 * Assumes stepsize to be 1. Can easily be changed for bigger stepsize.
 * Assumes that input range is a valid variation with respect to the values of start and upper.
 */
bool next_variation(int* const begin, int* end, int start, int upper)
{
    int* next = begin;
    while (next != end)
    {
        ++(*next);
        if (*next < upper)  // did we generate a valid variation by adding +1 to next?
        {
            return true;
        }
        (*next) = start;  //
        ++next;
    }
    return false;
}

/**
 * @brief prints all values in M(p, t, e_min, e_max)
 * @param p base of the floating point numbers
 * @param t length of the floating point numbers
 * @param e_min minimal exponent
 * @param e_max maximal exponent
 *
 */
void fp_numbers(int p, const int t, int e_min, int e_max)
{
    double res = 0, tmp = 0;
    int div = 0;
    int seq[t];
    printf("0.0\n");
    for (int i = e_min; i <= e_max; i++)
    {
        for (int j = 1; j < p; j++)
        {
            memset(seq, 0, sizeof(seq));  // (re)set to zero
            seq[0] = j;                   // leave out zero for the first place
            do
            {
                res = 0;
                div = 10;
                for (int k = 0; k < t; k++)
                {
                    res += (double) seq[k] / (double) div;
                    div *= 10;
                }
                tmp = res * pow(2, i);
                printf("%lf\n", tmp);
                printf("%lf\n", -tmp);
            } while (next_variation(seq + 1, seq + t, 0, p));
        }
    }
}

/**
 * @brief really simple parsing of commandline arguments
 * @param argc number of commandline arguments
 * @param argv commandline arguments
 * @param p base of the floating point numbers
 * @param t length of the floating point numbers
 * @param e_min minimal exponent
 * @param e_max maximal exponent
 * @returns wether reading was successful
 *
 * No checking for correctness of inputs.
 */
bool parse_commandline_args(int argc, char* argv[], int* p, int* t, int* e_min, int* e_max)
{
    if (argc == 5)
    {
        *p     = atoi(argv[1]);
        *t     = atoi(argv[2]);
        *e_min = atoi(argv[3]);
        *e_max = atoi(argv[4]);
    }
    else
    {
        return false;
    }
    return true;
}

int main(int argc, char* argv[])
{
    int p = 0, t = 0, e_min = 0, e_max = 0;
    bool s = parse_commandline_args(argc, argv, &p, &t, &e_min, &e_max);
    if (s)
    {
        fp_numbers(p, t, e_min, e_max);
    }
    else
    {
        printf("=== M(2, 4, -2, 4) ===\n");
        fp_numbers(2, 4, -2, 4);
        printf("=== M(3, 2, -1, 2) ===\n");
        fp_numbers(3, 2, -1, 2);
    }
    return 0;
}

#include <stdio.h>
#include <float.h>

// According to IEEE-754 and the implicit first digit of 1, the number 1 has a
// mantissa of all 0s, so that adding 1 to the mantissa does not have any impact
// on the exponent.
// Let's hope that our machine has the correct endianness, though, so that
// adding 1 in the integer representation indeed modifies the mantissa (and not
// the sign bit) in the floating point representation by 1 (and not by
// 2^(len(mantissa)-1)).
//
// If we are lucky, this implementation does not rely on the way 1+epsilon is
// rounded, as would the naive search for epsilon.
float s_eps()
{
    union
    {
        float f;
        int i;
    } val;
    val.f = 1;
    val.i++;
    return val.f - 1;
}

double d_eps()
{
    union
    {
        double f;
        long int i;
    } val;
    val.f = 1;
    val.i++;
    return val.f - 1;
}

int main()
{
    printf("%20.20f ~ %20.20f\n", s_eps(), FLT_EPSILON);
    printf("%20.20lf ~ %20.20lf\n", d_eps(), DBL_EPSILON);
}

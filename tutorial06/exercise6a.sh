#!/bin/sh
./exercise5.out 2 3 −1 3 | \
gnuplot -p -e "unset ytics;set xtic axis;set xzeroaxis lt -1 lw .5;unset border;\
plot [][-1:1] '-' using 1:(0) with points pointtype 2 title 'M(2,4,-2,4)'"
./exercise5.out 2 3 −1 3 | \
gnuplot -p -e "unset ytics;set xtic axis;set xzeroaxis lt -1 lw .5;unset border;set logsc x 2;\
plot [][-1:1] '-' using 1:(0) with points pointtype 2 title 'M(2,4,-2,4)'"

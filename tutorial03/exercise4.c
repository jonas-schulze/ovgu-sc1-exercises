#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// reduce divides *n by factor as many times as possible and returns that count.
size_t reduce(size_t* n, size_t factor)
{
    size_t power = 0, n_copy = *n;
    while (n_copy % factor == 0)
    {
        power += 1;
        n_copy /= factor;
    }
    *n = n_copy;
    return power;
}

// print prints the "factor^power" part of a prime factorization.
//
// * takes care of separating " * "
// * only works for a single factorization
void print(size_t factor, size_t power)
{
    static bool needs_seperator = false;
    if (needs_seperator)
    {
        printf(" * ");
    }
    printf("%lu^%lu", factor, power);
    needs_seperator = true;
}

int main()
{
    printf("Insert a positive number: ");
    size_t n;
    scanf("%lu", &n);
    printf("The prime factorization of %lu is:\n", n);
    // Edge case:
    if (n == 1)
    {
        printf("1\n");
        return EXIT_SUCCESS;
    }
    // This condition should evaluate mostly as false, therefore allowing the
    // CPU's branch prediction to get cracking. This also avoids the overhead
    // of back-and-forth copying done by `reduce`.
    if (n % 2 == 0)
    {
        print(2, reduce(&n, 2));
    }
    for (size_t factor = 3; n != 1; factor += 2)
    {
        if (n % factor == 0)
        {
            print(factor, reduce(&n, factor));
        }
    }
    printf("\n");
}

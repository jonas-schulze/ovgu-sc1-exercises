#include <stdio.h>

int round_double(double inp)
{
    // conversion from double to int always cuts off the decimal places, equivalent to floor
    int inp_conv = (int) inp;
    // check if we have to round up or down
    return (inp - (double) inp_conv < 0.5) ? inp_conv : inp_conv + 1;
}

int main(int argc, char const* argv[])
{
    double inp;
    printf("Enter positive floating point number: ");
    scanf("%lf", &inp);
    printf("Input rounded to integer: %i\n", round_double(inp));
    return 0;
}

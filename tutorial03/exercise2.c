#include <stdio.h>

// temp_t may hold a temperature in celsius or fahrenheit scale.
// Watch out for format strings: temp_t requires %f!
typedef float temp_t;

// f2c converts a given temperature in fahrenheit to celsius scale.
temp_t f2c(temp_t f)
{
    return (f - 32) * 5 / 9;
}

temp_t read_temp()
{
    temp_t t;
    scanf("%f", &t);
    return t;
}

int main()
{
    // part a:
    printf("Enter a temperature in fahrenheit scale: ");
    temp_t f = read_temp();
    printf("%f F ~= %f C\n", f, f2c(f));
    // part b:
    printf("Gimme a lower bound in F: ");
    temp_t lower = read_temp();
    printf("Gimme an upper bound in F: ");
    temp_t upper = read_temp();
    for (; lower <= upper; ++lower)
    {
        printf("%f F ~= %f C\n", lower, f2c(lower));
    }
}

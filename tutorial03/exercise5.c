#include <stdio.h>
#include <math.h>

// Calculates exponential function for x in [0,1].
//
// There is no checking wether x is in [0,1] and the function should never be called directly,
// instead expo should be used.
double expo_on_unit(double x, int max_i, double tol)
{
    double result = 1.0, tmp = 0.0, summand = 1.0;
    for (int i = 1; (i <= max_i) && fabs(result - tmp) > tol; i++)
    {
        tmp = result;
        summand *= x / i;  // x^i/i!
        result += summand;
    }
    return result;
}

// Function for calculating exponential function.
//
// We use the fact that exp(x+y) = exp(x)*exp(y).
// So we always call the exponential function for values between 0 and 1.
double expo(double x, int max_i, double tol)
{
    double result = 1.0;
    while (x > 1.0)
    {
        x -= 1.;
        result *= M_E;
    }
    while (x < 0.0)
    {
        x += 1.;
        result /= M_E;
    }
    return result * expo_on_unit(x, max_i, tol);
}

int main(int argc, char const* argv[])
{
    double inp, tol;
    int max_i;
    printf("Enter floating point number x: ");
    scanf("%lf", &inp);
    printf("Enter maximal number of summands: ");
    scanf("%i", &max_i);
    printf("Enter error tolerance: ");
    scanf("%lf", &tol);
    printf(
      "Better but without using exp(x+y) = exp(x)*exp(y), so for large values this will become "
      "incorrect:\nexp(x): %.17lf\n",
      expo_on_unit(inp, max_i, tol));
    printf("Most reliable exp(x): %.17lf\n", expo(inp, max_i, tol));
    return 0;
}

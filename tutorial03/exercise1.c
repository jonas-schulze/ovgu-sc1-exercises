#include <stdio.h>

int divide(int numerator, int denominator)
{
    // Without checking for division by zero a floating point exception is raised
    // More correctly the floating point exception should be called arithmetic exception, since it
    // covers all arithmetic errors
    //
    if (denominator == 0)
    {
        printf("Division by zero is not allowed, give input new value:\n");
        printf("Enter denominator: ");
        scanf("%i", &denominator);
    }
    return numerator / denominator;
}

double divide_d(double numerator, double denominator)
{
    // Without checking for division by zero a floating point exception is raised
    if (denominator == 0)
    {
        printf("Division by zero is not allowed, give input new value:\n");
        printf("Enter denominator: ");
        scanf("%lf", &denominator);
    }
    return numerator / denominator;
}

int mod(int numerator, int denominator)
{
    // Without checking for division by zero a floating point exception is raised
    if (denominator == 0)
    {
        printf("Division by zero is not allowed, give input new value:\n");
        printf("Enter denominator: ");
        scanf("%i", &denominator);
    }
    return numerator % denominator;
}

int main(int argc, char const* argv[])
{
    /* code */
    int n = 0, d = 0;
    // Division by zero raises a warning at compile time, but not an error (unless you set -Werror)
    // Of course this raises a floating point exception at run-time
    // int c = n/0;
    // Eventhough a = b mod d if a = b + dk is perfectly fine for d = 0,
    // it behaves the same as the division.
    // int c = n % 0;
    printf("First division with ints:\n");
    printf("Enter numerator: ");
    scanf("%i", &n);
    printf("Enter denominator: ");
    scanf("%i", &d);
    printf("%i\n", divide(n, d));
    printf("Now for doubles:\n");
    double n_d = 0, d_d = 0;
    printf("Enter numerator: ");
    scanf("%lf", &n_d);
    printf("Enter denominator: ");
    scanf("%lf", &d_d);
    printf("%lf\n", divide_d(n_d, d_d));
    return 0;
}

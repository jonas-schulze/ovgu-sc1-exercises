#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    scanf("%d", &n);
    // Edge cases:
    if (n < 0)
    {
        fprintf(stderr, "Negative number entered: %d\n", n);
        return EXIT_FAILURE;
    }
    if (n == 0)
    {
        printf("0\n");
        return EXIT_SUCCESS;
    }
    // Print the last digit as long as there is one:
    while (n != 0)
    {
        printf("%d", n % 10);
        n /= 10;
    }
    printf("\n");
    return EXIT_SUCCESS;
}

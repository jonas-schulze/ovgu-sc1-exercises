TUTORIALS = $(wildcard tutorial??)
ALMOST_EVERYTHING = $(wildcard tutorial??/*) Makefile Dockerfile

.PHONY: test $(TUTORIALS)

.container-build: $(ALMOST_EVERYTHING)
	docker build . --tag sc1
	# Create empty target file:
	touch $@

test: $(TUTORIALS)

$(TUTORIALS): .container-build
	@echo "=== Building $@/"
	# As we are building inside a container, technically we don't need the
	# `make clean`. But let's test it anyways.
	docker run --rm sc1 sh -c "cd $@ && make all -j && make clean"

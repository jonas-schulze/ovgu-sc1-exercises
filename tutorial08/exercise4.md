## Exercise 4
Calculating the scalar product $\langle v,\,x\rangle = v^\top x$ numerically results in the formula
\begin{equation*}
     \sum_{i=1}^n v_i x_i (1+\delta_i)\,.
\end{equation*}

* Every Multiplication introduces error $\delta_i$
* $\delta_i \leq u\;\forall i=1,\dots,\,n$
* Error propagates through the sum, thus can be estimated by geometric series
\begin{equation*}
    \sum_{i=1}^n v_i x_i (1+\delta_i)
    \leq \underbrace{\frac{1- u^n}{1- u}}_{=\epsilon_n} \sum_{i=1}^n v_i x_i = \epsilon_n \langle v,\, x\rangle
\end{equation*}
We can conclude for the absolute condition number
\begin{equation*}
    \vert y -\hat y\vert \leq \epsilon_n \lvert \langle v,\,\Delta x\rangle\rvert \leq \underbrace{\epsilon_n \lVert v\rVert}_{=c_{abs}} \lVert \Delta x \rVert
\end{equation*}
And equivalently for the relative condition number
\begin{equation*}
    \frac{\vert y -\hat y\vert}{\lvert y\rvert}
    \leq \frac{\epsilon_n \lvert \langle v,\,\Delta x\rangle\rvert}{\epsilon_n \lvert\langle v,\,x \rangle\rvert}
    \leq \underbrace{\frac{\epsilon_n \lVert v\rVert}{\epsilon_n \lVert v\rVert}}_{=c_{rel}} \frac{\lVert \Delta x \rVert}{\lVert x \rVert}
\end{equation*}

* If $\langle v,\,x\rangle$ is nearly zero the relative error can get very large
* If $\lVert v\rVert$ is very large absolute condition number gets large

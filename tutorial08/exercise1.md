---
title: Exercise 1
author:
- Jonas Schulze
- Nils Margenberg
header-includes:
  \usepackage{amsmath}
---

\newcommand{\abs}[1]{\lvert #1 \rvert}
\newcommand{\roundsingle}{\gamma_{\textup{s}}}
\newcommand{\rounddouble}{\gamma_{\textup{d}}}

# Part a

Let $a$ be $0.1\alpha_1\ldots\alpha_{52} \cdot 2^e$ with a suitable integer 
exponent $e$. Let's assume that $a$ has more than 23 binary digits and let
$j \geq 24$ be the minimal index such that $\alpha_j = 1$. Thus
$$
  a = 0.1\alpha_1\ldots\alpha_{23}\ldots 1\alpha_{j+1}\ldots\alpha_{52} \cdot 2^e
$$
and
\begin{align*}
  a_s &:= \roundsingle(0.1\alpha_1\ldots\alpha_{23} \cdot 2^e) \\
  r_a &:= \roundsingle(a-a_s) = \roundsingle(0.1\alpha_{j+1}\ldots\alpha_{\min(j+23, 52)} \cdot 2^{e-j})
\end{align*}
and $a_s, r_a$ are "regular numbers" iff first $e$ (to prevent $r_a = -\infty$)
and afterwards $e-j$ represent single precision exponents, i.e. $-125+j \leq e \leq 128$.

If $a$ does not have more than 23 binary digits, then $r_a = 0$ and we omit the
condition for $e+j$, i.e. $-125 \leq e \leq 128$.

# Part b

The more digits we lose, the bigger the error.
We lose the most digits if $j=24$, therefore
$$
  \abs{a-\rounddouble(a_s+r_a)}
= \abs{(0.\alpha_{48}\ldots\alpha_{52})_2 \cdot 2^{e-48}}
\leq (0.11111)_2 \cdot 2^{e-48}
= 0.96875 \cdot 2^{e-48}
$$
which in turn is maximized for $e = 1024$ (maximum exponent allowed).
Computing the corresponding absolute and relative error is left as an exercise
to the reader.

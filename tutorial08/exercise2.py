from sympy import *
init_printing()
u, c1, c2 = symbols("u c1 c2")
# One root of the polynomial is 0, we will calculate forward error for x near 0
p_direct = (1+u)*((1+u)**2*c1*u+c2*(1+u)**3*u**2)
p_horner = u*(1+u)*((1+u)*c1+c2*u*(1+u)**2)
p_exact = c1*u+c2*u**2

fwd_d = simplify((p_direct-p_exact)/p_exact)
fwd_h = simplify((p_horner-p_exact)/p_exact)
print("Forward error using direct evaluation:")
pprint(fwd_d)
print("Forward error using horner scheme:")
pprint(fwd_h)

---
title: Exercise 5
author:
- Jonas Schulze
- Nils Margenberg
header-includes:
  \usepackage{amsmath}
---

\newcommand{\R}{\mathbb{R}}
\newcommand{\LandauO}{\mathcal{O}}

\newcommand{\abs}[1]{\lvert #1 \rvert}

Let $f_\gamma$ be the algorithm retrieved from $f$ by applying the rounding
function $\gamma$ on each intermediate result. Thus, the forward analysis
determines $\hat{y} := f_\gamma(x)$, and the backward analysis considers
$\hat{y} = f(x+\Delta x)$.

Even though $a\in\R$ is considered to be fixed, it is to be rounded only in
the forward analysis. We define $\hat{a} := \gamma(a) = a(1+\delta_a)$ with
$\abs{\delta_a} \leq u$ and similarly for $\hat{x}$. If not stated otherwise,
any $\delta_{\mathrm{something}}$ is of magnitude $u$.

# Part a
Using
$$
  \hat{y}
  = \hat{a}\hat{x}(1+\delta)
  = ax(1+\delta_x)(1+\delta_a)(1+\delta)
$$
and $\hat{y} = a(x + \Delta x)$ we obtain
$$
  \Delta x = x\big( (1+\delta_a)(1+\delta_x)(1+\delta) - 1 \big)
$$
and
$$
  \frac{\abs{\Delta x}}{\abs{x}}
  = \abs{(1+\delta_a)(1+\delta_x)(1+\delta) - 1}
  \leq (1+u)^3-1
  = \LandauO(u)
$$
which is small by any means.
$\implies$ backwards stability $\implies$ stability.
Always.

# Part b
Using
$$
  \hat{y}
  = (\hat{a}+\hat{x})(1+\delta)
  = (a+x + a\delta_a + x\delta_x)(1+\delta)
$$
and $\hat{y} = a+(x+\Delta x)$ we obtain
$$
  \Delta x = a\delta_a + x\delta_x + (\hat{a}+\hat{x})\delta
$$
and
\begin{align*}
  \frac{\abs{\Delta x}}{\abs{x}}
  &= \left\lvert
    \frac{a}{x}\delta_a +
    \delta_x +
    \left( \frac{\hat{a}}{x} + (1+\delta_x) \right) \delta
  \right\rvert \\
  &= \left\lvert
    \frac{a}{x} \big( \delta_a + (1+\delta_a)\delta \big) +
    \delta_x +
    (1+\delta_x)\delta
  \right\rvert \\ 
  &\leq \frac{\abs{a}}{\abs{x}} (2u+u^2) + (2u+u^2)
\end{align*}
which for $\abs{x} \leq \frac{\abs{a} \cdot u}{\kappa}$ is at least
$2\kappa + \LandauO(u)$.

From the forward analysis shown in the lectures we know that conditioning
is especially bad for $x \approx -a$. We do not see this effect in the backward
analysis. So, in order to ensure (backward) stability we need
$\abs{x} \geq \abs{a} \cdot 2u$, such that
$\abs{\Delta x} / \abs{x} \leq 1 + \LandauO(u) \approx 1$.

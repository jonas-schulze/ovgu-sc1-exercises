---
title: Exercise 3
author:
- Jonas Schulze
- Nils Margenberg
header-includes:
  \usepackage{amsmath}
---

\let\to\longrightarrow
\let\mapsto\longmapsto

\newcommand{\Z}{\mathbb{Z}}

\newcommand{\abs}[1]{\lvert #1 \rvert}
\newcommand{\cabs}{c_{\textup{abs}}}
\newcommand{\crel}{c_{\textup{rel}}}

\newcommand{\EmptyDel}{\,\cdot\,}

According to the lecture notes, the local condition numbers are described by:
\begin{align*}
  \cabs(f,x) &= \abs{f'(x)} \\
  \crel(f,x) &= \cabs(f,x) \cdot \frac{\abs{x}}{\abs{f(x)}}
\end{align*}

# Part a
\begin{align*}
  \cabs(f,x) &= \abs{\cos(x)} \\
  \crel(f,x) &= \frac{\abs{x}}{\abs{\tan(x)}}
\end{align*}
which may cause problems for $\tan(x) \approx 0$, that is $x \approx k\pi$, and
for $\tan(x) \approx \pm\infty$, that is $x \approx (k+\tfrac{1}{2})\pi$, $k\in\Z$.
However, $x \to 0$ needs to be excluded from that list. Using that $\abs{\EmptyDel}$
is continuous and l'Hospital:
$$
  \lim_{x \to 0} \frac{x}{\tan(x)}
= \lim_{x \to 0} \frac{1}{\cos(x)^{-2}}
= \cos(0)^2 = 1
$$

# Part b
\begin{align*}
  \cabs(f,x) &= \frac{1}{\abs{1+x^2}} \\
  \crel(f,x) &= \frac{\abs{x}}{\abs{x^2+1}}\cdot\frac{1}{\abs{\arctan(x)}}
\end{align*}
which may cause problems for big $\abs{x}$ (big $\abs{x^2+1}$) and for
$\arctan(x) \approx 0$, that is $x \approx 0$. However, the latter is not the case:
$$
  \lim_{x \to 0} \frac{x}{x^2+1}\cdot\frac{1}{\arctan(x)}
  = \lim_{x \to 0} \frac{x}{\arctan(x)}
  = \lim_{x \to 0} \frac{1}{(x^2+1)^{-1}}
  = (0^2+1)^1 = 1
$$

# Part c
\begin{align*}
  \cabs(f,x) &= \abs{\tfrac{1}{2}\sqrt{e^x/x} + \tfrac{1}{2}\sqrt{xe^x}}
  = \abs{\tfrac{1}{2}\big(f(x)/x + f(x)\big)} \\
  \crel(f,x) &= \abs{\tfrac{1}{2}(1+x)}
\end{align*}
which may cause problems for large $\abs{x}$ (visible in $\crel$).


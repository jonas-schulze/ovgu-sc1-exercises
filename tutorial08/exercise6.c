#include <stdio.h>
#include <math.h>

float f1(float x)
{
    return exp(x);
}

float f2(float x)
{
    return exp(-x);
}

/**
 * @brief Integrate using midpoint rule on interval [a, a+h*n_points]; helper function
 */
float _integrate(float (*f)(float), int n_points, float h, float a)
{
    float result = 0.0;
    a += 0.5 * h;
    for (int i = 0; i < n_points; i++)
    {
        result += h * f(a + h * i);
    }
    return result;
}

/**
 * @brief Integrate using midpoint rule on interval [a, b]
 */
float integrate(float (*f)(float), int n_points, float a, float b)
{
    float result = 0.0;
    float h      = (b - a) / n_points;
    // split interval [a,b] in interval of length ≈ 1; rem_points are the remaining points
    // Maybe this could be optimized, but we often get an order of 2, which is what we expect
    int sub        = (int) (b - a);
    int sub_points = n_points / sub;
    int rem_points = n_points - sub_points * sub;
    // Integrate on subintervals and add up results
    for (int i = 0; i < sub; i++)
    {
        result += _integrate(f, sub_points, h, a);
        a += h * sub_points;
    }
    result += _integrate(f, rem_points, h, a);
    return result;
}

float rel_err(float app, float exa)
{
    return (app - exa) / exa;
}

int main()
{
    float exact = exp(20) - exp(-20);
    float approx[2];
    float rerr[2];
    float rerr_buffer[2] = {1, 1};
    int n[12] = {1024, 2048, 4096, 8192, 16384, 32768, 1000, 2000, 4000, 8000, 16000, 32000};
    for (int i = 0; i < 12; i++)
    {
        approx[0] = integrate(f1, n[i], -20, 20);
        printf("Using %d sampling points I_1 is %e\n", n[i], approx[0]);
        rerr[0] = rel_err(approx[0], exact);
        printf("Relative error: %e\n", rerr[0]);
        printf("Order of convergence: %e\n", rerr_buffer[0] / rerr[0] / 2);
        rerr_buffer[0] = rerr[0];
        printf("\n");
        approx[1] = integrate(f2, n[i], -20, 20);
        printf("Using %d sampling points I_2 is %e\n", n[i], approx[1]);
        rerr[1] = rel_err(approx[1], exact);
        printf("Relative error: %e\n", rerr[1]);
        printf("Order of convergence: %e\n", rerr_buffer[1] / rerr[1] / 2);
        rerr_buffer[1] = rerr[1];
        printf("\n");
    }
}

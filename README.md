# ovgu-sc1-exercises

Scientific Computing 1 exercise sheets


## Formatting code

For a nice and consistent code style, `clang-format` can be used.
It is part of the Clang tool from LLVM. If `clang` is already installed  you should have an executable named `clang-format`.
`clang-format` uses a style file to format your code. There are some predefined styles like Google or LLVM. However you
can also provide your own settings if the default ones do not fit your needs or flavour.

To format your source code run the following command:
```sh
> clang-format -i -style=file <FILE1> <FILE2> ...
```

The `-i` option will change the code *inplace*, so the files will be overwritten.
If you want to check the output of `clang-format` first, you can leave out the `-i` option
and `clang-format` will print to stdout. The setting `-style=file` (`file` is not an abbreviation for a filename) tells `clang-format` that we
will use our own style definition file. `clang-format` will search for a
file named `.clang-format` in the parent directories of your current directory.

[Vim](https://clang.llvm.org/docs/ClangFormat.html#vim-integration),
[Emacs](https://clang.llvm.org/docs/ClangFormat.html#emacs-integration),
[Visual Studio Code](https://code.visualstudio.com/docs/languages/cpp),
[Atom](https://atom.io/packages/clang-format) and probably more editors offer the opportunity
to run `clang-format` directly inside the editor. This is very comfortable, e.g. if there is the possibility to enable format on save.

You can also switch clang-format on and off in specific code sections with comments:
```cpp
//clang-format off
...
//clang-format on
```

## Docker

To build everything inside a Docker container, just run `make test` in the base directory.
This won't leave any build artifacts but a container image tagged `sc1`.
However, on subsequent container builds, old images are not removed.
They get untagged, thus causing some fuzz in `docker images`.
An image is about `716MB`, but all expensive layers are shared between subsequent container builds.

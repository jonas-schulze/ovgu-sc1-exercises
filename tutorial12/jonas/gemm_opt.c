#include "matrix.h"
#include <stdio.h>
#define alignment MATRIX_TILE_SIZE*sizeof(double)
#define MIN(a,b) (((a)<(b))?(a):(b))

#define transpose 0

void gemm_opt_4x4_transpose(
    //double alpha,
    double* __restrict__ At, // int lda,
    double* __restrict__ B, int ldb,
    double* __restrict__ C, int ldc)
{
  double * __restrict__ at = __builtin_assume_aligned(At, alignment);
  double * __restrict__ b = __builtin_assume_aligned(B, alignment);
  double * __restrict__ c = __builtin_assume_aligned(C, alignment);

  int i,j,k,c_addr,b_addr,a_addr;
  c_addr = b_addr = 0;
  for(j = 0; j < MATRIX_TILE_SIZE; j++) {
    a_addr = 0;
    for(i = 0; i < MATRIX_TILE_SIZE; i++) {
      for(k = 0; k < MATRIX_TILE_SIZE; k++) {
        c[c_addr] += at[a_addr+k] * b[b_addr+k];
        //c[e(i,j,ldc)] += at[e(k,i,lda)] * b[e(k,j,ldb)];
      }
      c_addr += 1;
      a_addr += MATRIX_SUPERTILE_SIZE_M;
    }
    c_addr += ldc-i;
    b_addr += ldb;
  }
}

void gemm_opt_4x4(
    //double alpha,
    double* __restrict__ A, // int lda,
    double* __restrict__ B, int ldb,
    double* __restrict__ C, int ldc)
{
  double * __restrict__ a = __builtin_assume_aligned(A, alignment);
  double * __restrict__ b = __builtin_assume_aligned(B, alignment);
  double * __restrict__ c = __builtin_assume_aligned(C, alignment);

  int i,j,k,c_addr,b_addr,a_addr;

  for(i = 0; i < MATRIX_TILE_SIZE; i++) {
    c_addr = e(i,0,ldc);
    b_addr = 0;
    for(j = 0; j < MATRIX_TILE_SIZE; j++) {
      a_addr = i;
      for(k = 0; k < MATRIX_TILE_SIZE; k++) {
        c[c_addr] += a[a_addr] * b[b_addr+k];
        a_addr += MATRIX_SUPERTILE_SIZE_M;
      }
      c_addr += ldc;
      b_addr += ldb;
    }
  }
}

void gemm_opt_16x16(
    int m, int p, int n,
    double alpha,
    double * __restrict__ A, int lda,
    double * __restrict__ B, int ldb,
    double * __restrict__ C, int ldc)
{
  double * __restrict__ a = __builtin_assume_aligned(A, alignment);
  double * __restrict__ b = __builtin_assume_aligned(B, alignment);
  double * __restrict__ c = __builtin_assume_aligned(C, alignment);

  int i,j,k;

  // Copy and scale a super tile.
  int x,y,addr1,addr2;
  static double tmp[MATRIX_SUPERTILE_SIZE_M*MATRIX_SUPERTILE_SIZE_N] __attribute__((aligned));
  for(k = 0; k < n; k+=MATRIX_TILE_SIZE) {
    for(i = 0; i < m; i+=MATRIX_TILE_SIZE) {
      // Copy the whole tile. This needs the tile to be properly set to zero,
      // if it is partially padded.
#if transpose
      // Transpose only on the tile level, i.e. tile (i,j) remains at position
      // (i,j) but its elements are to be transposed.
      addr1 = e(i,k,MATRIX_SUPERTILE_SIZE_M);
      addr2 = e(i,k,lda);
      for(x = 0; x < MATRIX_TILE_SIZE; x++) {
        for(y = 0; y < MATRIX_TILE_SIZE; y++) {
          tmp[addr1] = a[addr2+y] * alpha;
          addr1 += MATRIX_SUPERTILE_SIZE_M;
          //tmp[e(i+x,k+y,MATRIX_SUPERTILE_SIZE)] = a[e(i+y,k+x,lda)] * alpha;
        }
        // Go MATRIX_TILE_SIZE columns back and 1 row further down:
        addr1 += 1 - MATRIX_TILE_SIZE*MATRIX_SUPERTILE_SIZE_M;
        // Go 1 column ahead:
        addr2 += lda;
      }
#else
      addr1 = e(i,k,MATRIX_SUPERTILE_SIZE_M);
      addr2 = e(i,k,lda);
      for(x = 0; x < MATRIX_TILE_SIZE; x++) {
        for(y = 0; y < MATRIX_TILE_SIZE; y++) {
          tmp[addr1+y] = a[addr2+y] * alpha;
        }
        addr1 += MATRIX_SUPERTILE_SIZE_M;
        addr2 += lda;
      }
#endif
    }
  }

#if transpose
  for(j = 0; j < p; j+=MATRIX_TILE_SIZE) {
    for(i = 0; i < m; i+=MATRIX_TILE_SIZE) {
      for(k = 0; k < n; k+=MATRIX_TILE_SIZE) {
        // The position of the tiles didn't change, its elements did:
        gemm_opt_4x4_transpose(
            //alpha,
            tmp+e(i,k,MATRIX_SUPERTILE_SIZE_M), // MATRIX_SUPERTILE_SIZE,
            b+e(k,j,ldb), ldb,
            c+e(i,j,ldc), ldc);
      }
    }
  }
#else
  double * __restrict__ b_tmp = b;
  for(j = 0; j < p; j+=MATRIX_TILE_SIZE) {
    for(k = 0; k < n; k+=MATRIX_TILE_SIZE) {
      for(i = 0; i < m; i+=MATRIX_TILE_SIZE) {
        gemm_opt_4x4(
            //alpha,
            tmp+e(i,k,MATRIX_SUPERTILE_SIZE_M), // MATRIX_SUPERTILE_SIZE,
            b_tmp, ldb,
            c+e(i,j,ldc), ldc);
      }
      // MATRIX_TILE_SIZE rows down:
      b_tmp += MATRIX_TILE_SIZE;
    }
    // MATRIX_TILE_SIZE columns ahead and all the rows up, again:
    b_tmp += ldb*MATRIX_TILE_SIZE - k;
  }
#endif
}

void gemm_opt(int m, int p, int n,
             double alpha,
             double * __restrict__ A, int lda,
             double * __restrict__ B, int ldb,
             double beta,
             double * __restrict__ C, int ldc)
{
  double * a = __builtin_assume_aligned(A, MATRIX_ALIGNMENT);
  double * b = __builtin_assume_aligned(B, MATRIX_ALIGNMENT);
  double * c = __builtin_assume_aligned(C, MATRIX_ALIGNMENT);

  const int end = p*ldc;
  for(int i = 0; i < end; i++) {
    c[i] *= beta;
  }

  int i,j,k;
  int m_tile, n_tile, p_tile = MATRIX_SUPERTILE_SIZE_P;
  for(j = 0; j < p; j+=MATRIX_SUPERTILE_SIZE_P) {
    p_tile = MIN(p-j,p_tile);
    n_tile = MATRIX_SUPERTILE_SIZE_N;
    for(k = 0; k < n; k+=MATRIX_SUPERTILE_SIZE_N) {
      n_tile = MIN(n-k,n_tile);
      m_tile = MATRIX_SUPERTILE_SIZE_M;
      for(i = 0; i < m; i+=MATRIX_SUPERTILE_SIZE_M) {
        m_tile = MIN(m-i,m_tile);

        gemm_opt_16x16(
            m_tile, p_tile, n_tile,
            alpha,
            a+e(i,k,lda), lda,
            b+e(k,j,ldb), ldb,
            c+e(i,j,ldc), ldc);
      }
    }
  }
}


void gemm_blas(const int m, const int n, const int k,
               const double alpha, double* a, const int lda, double* b, const int ldb,
               const double beta, double* c, const int ldc)
{
  char trans = 'N';
  dgemm_(&trans, &trans, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);
}

#undef MAX
#undef alignment

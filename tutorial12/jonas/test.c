#include <criterion/criterion.h>
// https://github.com/Snaipe/Criterion

#include "matrix.h"

void copy_raw_matrix(matrix* m, double* data);
void init_matrices(matrix* A, matrix* B, matrix* C);
void clear_matrices(matrix* A, matrix* B, matrix* C);
void log_matrix(matrix* m);

Test(simple, test)
{
    int m, n, p;
    m = n = p = 3;

    matrix A, B, C;
    init_matrices(&A, &B, &C);
    double alpha = 2;
    double beta  = 3;

    double expected[9] = {2, 2, 2, 3, 2, 2, 3, 3, 2};
    gemm_opt(3, 3, 3, alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    for (int row = 0; row < m; row++)
    {
        for (int col = 0; col < p; col++)
        {
            int i = e(row, col, C.ld),
                j = e(row, col, C.rows);
            cr_expect_float_eq(C.data[i], expected[j], 1e-12,
                               "mismatch at position (%d,%d): expected %g, got %g\n",
                               row, col, expected[i], C.data[j]);
        }
    }
    cr_log_info("C = \n"); log_matrix(&C);

    clear_matrices(&A, &B, &C);
}

Test(simple, test_with_reset)
{
    int m, n, p;
    m = n = p = 3;

    matrix A, B, C;
    init_matrices(&A, &B, &C);
    double alpha = 2;
    double beta  = 0;  // reset C

    double expected[9] = {2, 2, 2, 0, 2, 2, 0, 0, 2};
    gemm_opt(3, 3, 3, alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    for (int row = 0; row < m; row++)
    {
        for (int col = 0; col < p; col++)
        {
            int i = e(row, col, C.ld),
                j = e(row, col, C.rows);
            cr_expect_float_eq(C.data[i], expected[j], 1e-12,
                               "mismatch at position (%d,%d): expected %g, got %g\n",
                               row, col, expected[i], C.data[j]);
        }
    }
    cr_log_info("C = \n"); log_matrix(&C);

    clear_matrices(&A, &B, &C);
}

Test(simple, slightly_bigger)
{
    int m, n, p;
    m = p = 8;
    n = 12;

    matrix A, B, C;
    int status[3] = { matrix_new(&A, m, n),
                      matrix_new(&B, n, p),
                      matrix_new(&C, m, p) };
    cr_log_info("initializing matrices, status: %d, %d, %d\n", status[0], status[1], status[2]);
    cr_log_info("leading dimensions: %d, %d, %d\n", A.ld, B.ld, C.ld);

    double a[8*12] = {0.760088, 0.0353629, 0.227648, 0.332398, 0.182725, 0.422502, 0.472859, 0.605669, 0.156424, 0.318611, 0.316208, 0.797212, 0.623946, 0.783727, 0.347655, 0.376598, 0.144187, 0.593707, 0.37075, 0.146015, 0.443917, 0.533722, 0.36189, 0.38311, 0.887515, 0.454287, 0.464108, 0.733284, 0.966521, 0.367702, 0.307906, 0.37247, 0.183212, 0.199862, 0.964672, 0.187105, 0.448109, 0.14589, 0.604899, 0.678644, 0.23306, 0.315689, 0.714703, 0.305528, 0.351331, 0.201283, 0.583211, 0.851284, 0.261523, 0.614805, 0.806549, 0.808428, 0.744218, 0.808217, 0.332469, 0.688439, 0.537496, 0.822921, 0.397009, 0.578486, 0.81219, 0.501668, 0.234602, 0.986954, 0.438379, 0.718351, 0.00342013, 0.208025, 0.869229, 0.627317, 0.267798, 0.0684205, 0.397082, 0.266282, 0.0998176, 0.430842, 0.651086, 0.945636, 0.793279, 0.925228, 0.810117, 0.73752, 0.438611, 0.437827, 0.42489, 0.710171, 0.741624, 0.857943, 0.703407, 0.26584, 0.276646, 0.92995, 0.31699, 0.572957, 0.328972, 0.258757};
    double b[12*8] = {0.563549, 0.920751, 0.660008, 0.515266, 0.241802, 0.940873, 0.486471, 0.0896694, 0.182724, 0.212763, 0.227753, 0.349321, 0.866592, 0.703919, 0.805356, 0.116302, 0.0860809, 0.897587, 0.506258, 0.0752335, 0.115525, 0.811051, 0.869503, 0.733528, 0.943676, 0.794584, 0.322816, 0.174903, 0.722192, 0.294436, 0.607336, 0.35276, 0.570427, 0.336535, 0.74664, 0.373705, 0.38808, 0.952061, 0.240269, 0.798624, 0.539432, 0.513892, 0.830711, 0.419743, 0.173374, 0.654963, 0.690011, 0.870958, 0.432507, 0.569376, 0.436087, 0.0777042, 0.697004, 0.913903, 0.706022, 0.242645, 0.167832, 0.995628, 0.0620038, 0.164899, 0.992755, 0.844628, 0.147957, 0.903951, 0.769626, 0.917582, 0.269247, 0.857287, 0.424778, 0.195258, 0.494546, 0.991306, 0.00874643, 0.64098, 0.273014, 0.0397715, 0.365132, 0.790153, 0.671804, 0.0225713, 0.277507, 0.456726, 0.156219, 0.336166, 0.60477, 0.616868, 0.486964, 0.944861, 0.556633, 0.867539, 0.696115, 0.755279, 0.662721, 0.608925, 0.618796, 0.961409};
    double c[8*8] = {0.51932, 0.848178, 0.0287595, 0.233402, 0.0234283, 0.777697, 0.314273, 0.514381, 0.984673, 0.640208, 0.684521, 0.714791, 0.65203, 0.672457, 0.772861, 0.25245, 0.483595, 0.170353, 0.685869, 0.787871, 0.755298, 0.960943, 0.633207, 0.487931, 0.583515, 0.038782, 0.371663, 0.744974, 0.598388, 0.925214, 0.836184, 0.870978, 0.021667, 0.771702, 0.72037, 0.0392199, 0.987276, 0.53574, 0.927495, 0.114392, 0.684774, 0.560249, 0.730667, 0.574812, 0.404955, 0.118462, 0.299688, 0.286794, 0.442547, 0.886735, 0.825792, 0.020756, 0.745824, 0.346821, 0.89909, 0.0514601, 0.733794, 0.93285, 0.243734, 0.521789, 0.668841, 0.590094, 0.060297, 0.990803};
    double alpha = 0.067042, beta = 0.60416;

    copy_raw_matrix(&A, a);
    copy_raw_matrix(&B, b);
    copy_raw_matrix(&C, c);

    double expected[8*8] = {0.458478, 0.653647, 0.181988, 0.323884, 0.205033, 0.660393, 0.348322, 0.513003, 0.794628, 0.56076, 0.587931, 0.648505, 0.601078, 0.668263, 0.68843, 0.426138, 0.482852, 0.27777, 0.593408, 0.675843, 0.677351, 0.815954, 0.575833, 0.539788, 0.57757, 0.223759, 0.437943, 0.725723, 0.634478, 0.83475, 0.720554, 0.808334, 0.136486, 0.602181, 0.612239, 0.190194, 0.793025, 0.528397, 0.745767, 0.315659, 0.679076, 0.546492, 0.671758, 0.624613, 0.528412, 0.32685, 0.405508, 0.477521, 0.353622, 0.647313, 0.633014, 0.15228, 0.600257, 0.366587, 0.671349, 0.194265, 0.715999, 0.813069, 0.386634, 0.609622, 0.729296, 0.659917, 0.281443, 0.92487};
    gemm_opt(m, p, n, alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    for (int row = 0; row < m; row++)
    {
        for (int col = 0; col < p; col++)
        {
            int i = e(row, col, C.ld),
                j = e(row, col, C.rows);
            cr_expect_float_eq(C.data[i], expected[j], 1e-4,
                               "mismatch at position (%d,%d): expected %g, got %g\n",
                               row, col, expected[i], C.data[j]);
        }
    }

    clear_matrices(&A, &B, &C);
}

void copy_raw_matrix(matrix* m, double* data)
{
    for (int row = 0; row < m->rows; row++)
    {
        for (int col = 0; col < m->cols; col++)
        {
            m->data[e(row, col, m->ld)] = data[e(row, col, m->rows)];
        }
    }
}

void init_matrices(matrix* A, matrix* B, matrix* C)
{
    int status[3] = { matrix_new(A, 3, 3),
                      matrix_new(B, 3, 3),
                      matrix_new(C, 3, 3) };
    cr_log_info("initializing matrices, status: %d, %d, %d\n", status[0], status[1], status[2]);
    cr_log_info("leading dimensions: %d, %d, %d\n", A->ld, B->ld, C->ld);

    double a[9] = {1, 1, 0, 0, 1, 1, 0, 0, 1};
    double b[9] = {1, 0, 1, 0, 1, 0, 0, 0, 1};
    double c[9] = {0, 0, 0, 1, 0, 0, 1, 1, 0};
    copy_raw_matrix(A, a);
    copy_raw_matrix(B, b);
    copy_raw_matrix(C, c);

    cr_log_info("A = \n"); log_matrix(A);
    cr_log_info("B = \n"); log_matrix(B);
    cr_log_info("C = \n"); log_matrix(C);
}

void clear_matrices(matrix* A, matrix* B, matrix* C)
{
    matrix_clear(A);
    matrix_clear(B);
    matrix_clear(C);
}

void log_matrix(matrix* m)
{
  for (int row = 0; row < 3; row++) {
    cr_log_info("  %1.0lf %1.0lf %1.0lf | %1.0lf\n",
                m->data[e(row, 0, m->ld)],
                m->data[e(row, 1, m->ld)],
                m->data[e(row, 2, m->ld)],
                m->data[e(row, 3, m->ld)]);
  }
  cr_log_info("  ------+---\n");
  cr_log_info("  %1.0lf %1.0lf %1.0lf | %1.0lf\n",
              m->data[e(3, 0, m->ld)],
              m->data[e(3, 1, m->ld)],
              m->data[e(3, 2, m->ld)],
              m->data[e(3, 3, m->ld)]);
}

#include <stdio.h>
#include <stdlib.h>
/* sys/time.h defines the gettimeofday function   */
#include <sys/time.h>

#include "logging_helpers.h"
#include "matrix.h"

#define INFO(message...) fprintf(stderr, message);

void init(matrix* a, matrix* b, matrix* c, int m, int n, int p)
{
  // just to be safe:
  static int count = 0;
  if (count > 0) {
    matrix_clear(a);
    matrix_clear(b);
    matrix_clear(c);
  }
  // allocate new memory (potentially smaller matrices, better locality)
  // and flush everything:
  matrix_new(a, m, n);
  matrix_new(b, n, p);
  matrix_new(c, m, p);
  // randomize only inputs:
  matrix_randomize(a);
  matrix_randomize(b);

  count++;
}

/**
 * The wtime functions returns the number of seconds ( up to a precision
 * of a microsecond, depending on the operating systems ) since the EPOCH
 * date ( https://de.wikipedia.org/wiki/Unixzeit, 1/1/1970 00:00).
 * Details about gettimeofday are in the manpage ( man gettimeofday ).
 * By encapsulating a piece of code with two calls to wtime one can dertermine
 * the runtime of the encapsulated code.
 **/
double wtime() {
	struct timeval tv;
	gettimeofday (&tv, NULL);
	return tv.tv_sec + tv.tv_usec / 1e6;
}

int main()
{
  int dim[] = {100, 500, 1000, 2000};
  int updates[] = {32, 64};

  double alpha = 1, beta = 0;
  int m, n, p;
  matrix A,B,C;

  double tic, toc;

  INFO("=== Square Problems\n");
  for (int i = 0; i < 3; i++) {
    m = n = p = dim[i];
    init(&A, &B, &C, m, n, p);

    tic = wtime();
    gemm_opt(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
    toc = wtime();
    log_flops(tic, toc, flop(m,n,p), 0);
    printf(" %4d %4d %4d\n", m, n, p);
  }

  for (int j = 0; j < 2; j++) {
    n = updates[j];
    INFO("=== Rank %d updates\n", n);
    for (int i = 0; i < 4; i++) {
      m = p = dim[i];
      init(&A, &B, &C, m, n, p);

      tic = wtime();
      gemm_opt(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
      toc = wtime();
      log_flops(tic, toc, flop(m,n,p), 0);
      printf(" %4d %4d %4d\n", m, n, p);
    }
  }

  INFO("=== Cleanup\n");
  matrix_clear(&A);
  matrix_clear(&B);
  matrix_clear(&C);
}

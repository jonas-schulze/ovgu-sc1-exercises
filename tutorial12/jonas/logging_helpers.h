#pragma once

/**
 * flop computes the number of floating point operations needed to perform
 * C <- alpha A B + beta C
 **/
double flop(int m, int n, int p);

/**
 * log_flops logs the elapsed time and Gflop/s
 * @param newline int whether to put a newline at the end
 **/
void log_flops(double tic, double toc, double ops, int newline);

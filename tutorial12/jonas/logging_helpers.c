#include <stdio.h>

double flop(int m, int n, int p)
{
  double ops = m*p;
  // for each entry of C
  ops *= 3*n + 1;
  // 2n multiplications in alpha * A * B,
  // n additions,
  // 1 multiplication by beta
  return ops;
}

void log_flops(double tic, double toc, double ops, int newline)
{
  double delta = toc - tic;
  double flops = ops / delta / 1e9;
  // Interesting .. :thinking:
  // https://stackoverflow.com/questions/15268088/printf-format-float-with-padding#15268129
  printf("%9.5lf seconds; %9.5lf Gflop/s;", delta, flops);
  if (newline) putchar('\n');
}

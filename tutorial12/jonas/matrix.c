#include <stdlib.h> // aligned_alloc() or posix_memalign
#include <stdio.h>

#include "matrix.h"

/**
 * grow increases x to be a multiple of alignment.
 **/
int grow(int x, int alignment)
{
  if (x % alignment != 0) {
    x /= alignment;
    x += 1;
    x *= alignment;
  }
  return x;
}

int matrix_new(matrix* m, int rows, int cols)
{
  m->rows = rows;
  m->cols = cols;

  // Grow matrix to be compatible with tiling.
  cols = grow(cols, MATRIX_TILE_SIZE);
  rows = grow(rows, MATRIX_TILE_SIZE);
  m->ld = rows;

  // Grow size to be compatible with posix_memalign.
  long size = rows*cols*sizeof(double);
  size = grow(size, MATRIX_ALIGNMENT);

  void* data;
  int status =  posix_memalign(&data, MATRIX_ALIGNMENT, size);
  m->data = (double*) data;

  // Flush everything:
  for(int i = 0; i < size/sizeof(double); i++) {
    m->data[i] = 0;
  }
  return status;
}

void matrix_clear(matrix* m)
{
  free(m->data);
}

void matrix_view(matrix* view, matrix* m, int row, int col)
{
  view->rows = MATRIX_TILE_SIZE;
  view->cols = MATRIX_TILE_SIZE;
  view->ld = m->ld;
  view->data = m->data + col*m->ld + row;
}

void matrix_randomize(matrix* m)
{
  for(int r = 0; r < m->rows; r++) {
    for(int c = 0; c < m->cols; c++) {
      m->data[e(r,c,m->ld)] = drand48();
    }
  }
}

void matrix_show(matrix* m)
{
  int row, col, pad = grow(m->cols, MATRIX_TILE_SIZE);
  for (row = 0; row < m->rows; row++) {
    printf("   ");
    for (col = 0; col < m->cols; col++) {
      printf("  %2.1f", m->data[e(row, col, m->ld)]);
    }
    printf("  |");
    for (;col < pad; col++) {
      printf("  %2.1f", m->data[e(row, col, m->ld)]);
    }
    putchar('\n');
  }
  printf("     ---\n");
  for (;row < m->ld; row++) {
    printf("   ");
    for (col = 0; col < m->cols; col++) {
      printf("  %2.1f", m->data[e(row, col, m->ld)]);
    }
    printf("  |");
    for (;col < pad; col++) {
      printf("  %2.1f", m->data[e(row, col, m->ld)]);
    }
    putchar('\n');
  }
}

#include <stdio.h>
#include <stdlib.h>
/* sys/time.h defines the gettimeofday function   */
#include <sys/time.h>

#include "logging_helpers.h"
#include "matrix.h"

#define INFO(message...) fprintf(stderr, message);

double frobenius(matrix* m1, matrix* m2)
{
  double norm = 0, tmp;
  for(int i = 0; i < m1->ld*m1->cols; i++) {
    tmp = m1->data[i] - m2->data[i];
    norm += tmp*tmp;
  }
  return norm;
}

/**
 * The wtime functions returns the number of seconds ( up to a precision
 * of a microsecond, depending on the operating systems ) since the EPOCH
 * date ( https://de.wikipedia.org/wiki/Unixzeit, 1/1/1970 00:00).
 * Details about gettimeofday are in the manpage ( man gettimeofday ).
 * By encapsulating a piece of code with two calls to wtime one can dertermine
 * the runtime of the encapsulated code.
 **/
double wtime() {
	struct timeval tv;
	gettimeofday (&tv, NULL);
	return tv.tv_sec + tv.tv_usec / 1e6;
}

int main()
{
  // ===========================================================================
  INFO("=== Initialize data\n");
  int ld = 1000;
  INFO("dim=%d\n", ld);
  int size = ld*ld;

  matrix A,B,C,D;
  INFO("%d\n", matrix_new(&A, ld, ld));
  INFO("%d\n", matrix_new(&B, ld, ld));
  INFO("%d\n", matrix_new(&C, ld, ld));
  INFO("%d\n", matrix_new(&D, ld, ld));

  matrix_randomize(&A);
  matrix_randomize(&B);

  // ===========================================================================
  INFO("=== Perform our gemm\n");
  int m,n,p;
  m = n = p = ld;

  double alpha = 1, beta = 0;

  // Ours
  double tic, toc;
  double time, err;

  tic = wtime();
  gemm_opt(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
  toc = wtime();
  log_flops(tic, toc, flop(m,n,p), 1);
  time = toc-tic;

  INFO("=== Perform OpenBLAS gemm\n");
  double tic1, toc1;
  tic1 = wtime();
  gemm_blas(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, D.data, D.ld);
  toc1 = wtime();
  log_flops(tic1, toc1, flop(m,n,p), 1);
  INFO("=== Compare\n");
  INFO("OpenBLAS is %lf times faster than the custom implementation\n",
         (toc-tic)/(toc1 -tic1));
  err = frobenius(&C, &D);
  INFO("ERROR: %g\n", err);
  
  // ===========================================================================
  INFO("=== Cleanup\n");
  matrix_clear(&A);
  matrix_clear(&B);
  matrix_clear(&C);
  matrix_clear(&D);
}

#include <stdlib.h> // aligned_alloc() or posix_memalign
#include <unistd.h> // sysconf()
#include <stdio.h>
#include <math.h>

#include "matrix.h"

/**
 * grow increases x to be a multiple of alignment.
 **/
int grow(int x, int alignment)
{
  if (x % alignment != 0) {
    x /= alignment;
    x += 1;
    x *= alignment;
  }
  return x;
}

int matrix_new(matrix* m, int rows, int cols)
{
  m->rows = rows;
  m->cols = cols;

  // Grow matrix to be compatible with tiling.
  cols = grow(cols, TILE_SIZE);
  rows = grow(rows, TILE_SIZE);
  m->ld = rows;

  // Grow size to be compatible with posix_memalign.
  //const long pagesize = sysconf(_SC_PAGESIZE);
  long size = rows*cols*sizeof(double);
  size = grow(size, MATRIX_ALIGNMENT);

  void* data;
  int status =  posix_memalign(&data, MATRIX_ALIGNMENT, size);
  m->data = (double*) data;
  for (int i =0; i < size/sizeof(double); i++) {
    m->data[i] = 0;
  }
  return status;
}

void matrix_clear(matrix* m)
{
  free(m->data);
}

void matrix_view(matrix* view, matrix* m, int row, int col)
{
  view->rows = TILE_SIZE;
  view->cols = TILE_SIZE;
  view->ld = m->ld;
  view->data = m->data + col*m->ld + row;
}

void matrix_randomize(matrix* m)
{
  srand(1);
  // Sets potentially padded area as well. Make sure to not time this.
  //int end = (m->cols-1)*(m->ld) + m->rows;
  //for(int i = 0; i < end; i++) {
  //  m->data[i] = drand48();
  //}
  for (int j=0;j<m->cols;j++){
    for (int i=0;i<m->rows;i++){
      m->data[e(i,j,m->ld)] = drand48();
    }
  }
}

void matrix_show(matrix* m)
{
  for (int row = 0; row < m->rows; row++) {
    printf("   ");
    for (int col = 0; col < m->cols; col++) {
      printf("  %2.1f", m->data[e(row, col, m->ld)]);
    }
    putchar('\n');
  }
}

double matrix_compare(matrix* a, matrix* b){
  double err=0;
  for (int j = 0; j < a->cols; ++j) {
    for (int i = 0; i < a->rows; ++i) {
      err += (a->data[e(i,j,a->ld)] - b->data[e(i,j,b->ld)])*(a->data[e(i,j,a->ld)] - b->data[e(i,j,b->ld)]);
    }
  }
  return sqrt(err);
}

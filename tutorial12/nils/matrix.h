#pragma once

void gemm_blas(const int m, const int n, const int k,
               const double alpha, double* a, const int lda, double* b, const int ldb,
               const double beta, double* c, const int ldc);

void dgemm_(const char* TRANSA, const char* TRANSB, const int* M, const int* N,
           const int* K, const double* ALPHA, const double* A, const int* LDA,
           const double* B, const int* LDB, const double* BETA, double* C, const int* LDC);


/**
 * gemm_opt computes the matrix product C <- alpha A B + beta C.
 *
 * @param alpha scalar
 * @param A m-by-n matrix
 * @param B n-by-p matrix
 * @param beta scalar
 * @param C m-by-p matrix
 **/
void gemm_opt(int m, int p, int n, double alpha,
             const double *A, int lda, const double *B, int ldb,
             double beta, double *C, int ldc);

#define MATRIX_ALIGNMENT 4096
#define TILE_SIZE 8

#define STILE_SIZE_M 256
#define STILE_SIZE_N 128
#define STILE_SIZE_P 1024

#define INT int
/**
 * @param r row
 * @param c column
 * @param ld leading dimension
 **/
#define e(r, c, ld) ((r) + (c)*(ld))

typedef struct {
  INT rows;
  INT cols;
  INT ld; // leading dimension
  double* data;
} matrix;

/**
 * matrix_new allocates storage for a rows-by-cols matrix. Strategies:
 *
 *   1. Global page alignment
 *   2. Grow matrix to multiples of MATRIX_TILE_SIZE
 *
 * Don't:
 *
 *   1. Column-wise page alignment: cuts performance in half compared to
 *      strategies 1+2 using 4x4 tiles.
 *   2. Modify tile size: values of 3 or 4 each cut performance in half.
 *
 * @returns status returned by posix_memalign
 **/
int matrix_new(matrix* m, int rows, int cols);

/**
 * matrix_clear frees all memory held by m.
 * Must not be called on a matrix view.
 **/
void matrix_clear(matrix* m);

/**
 * matrix_view creates a view of m in view
 *
 * @param view (output)
 * @param m
 * @param row of first view element
 * @param col of first view element
 **/
void matrix_view(matrix* view, matrix* m, int row, int col);

/**
 * matrix_randomize puts random number in each matrix entry
 **/
void matrix_randomize(matrix* m);

void matrix_show(matrix* m);

double matrix_compare(matrix* a, matrix* b);

#include <stdio.h>

double flop(int m, int n, int p)
{
  double ops = m*p;
  // for each entry of C
  ops *= 3*n + 1;
  // 2n multiplications in alpha * A * B,
  // n additions,
  // 1 multiplication by beta
  return ops;
}

void log_flops(double tic, double toc, double ops)
{
  double delta = toc - tic;
  printf("%lg seconds\n", delta);
  printf("%lg Gflop/s\n", ops / delta / 1e9 );
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* sys/time.h defines the gettimeofday function   */
#include <sys/time.h>
#include "logging_helpers.h"
#include "matrix.h"

#define INFO(message) printf(message);

/**
 * The wtime functions returns the number of seconds ( up to a precision
 * of a microsecond, depending on the operating systems ) since the EPOCH
 * date ( https://de.wikipedia.org/wiki/Unixzeit, 1/1/1970 00:00).
 * Details about gettimeofday are in the manpage ( man gettimeofday ).
 * By encapsulating a piece of code with two calls to wtime one can dertermine
 * the runtime of the encapsulated code.
 **/
double wtime() {
	struct timeval tv;
	gettimeofday (&tv, NULL);
	return tv.tv_sec + tv.tv_usec / 1e6;
}

int main()
{
  // ===========================================================================
  INFO("=== Initialize data\n");
  int dim[] = {100, 500, 1000};
  int ld = 1024;
  int size = ld*ld;

  matrix A,B,C,TMP;
  printf("%d\n", matrix_new(&A, ld, ld));
  printf("%d\n", matrix_new(&B, ld, ld));
  printf("%d\n", matrix_new(&C, ld, ld));
  printf("%d\n", matrix_new(&TMP, ld, ld));
  matrix_randomize(&A);
  matrix_randomize(&B);
  matrix_randomize(&C);
  for (int i = 0; i < size; ++i) {
    TMP.data[i] = C.data[i];
  }
  // ===========================================================================
  INFO("=== Perform gemm\n");
  int m,n,p;
  m = n = p = ld;
  //matrix_show(&A);
  //matrix_show(&B);
  //matrix_show(&C);
  double alpha = 1.3, beta = 1.7;
  
  // Ours
  double tic, toc;
  tic = wtime();
  gemm_opt(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, C.data, C.ld);
  toc = wtime();
  log_flops(tic, toc, flop(m,n,p));
#ifndef NOCOMP  
  INFO("=== Compare to OpenBLAS\n");
  double tic1, toc1;
  tic1 = wtime();
  gemm_blas(m,p,n,alpha, A.data, A.ld, B.data, B.ld, beta, TMP.data, TMP.ld);
  toc1 = wtime();
  log_flops(tic1, toc1, flop(m,n,p));
  printf("OpenBLAS is %lf times faster than the custom implementation\n",
         (toc-tic)/(toc1 -tic1));
  printf("Error between custom and OpenBLAS: %lf\n", matrix_compare(&TMP,&C));
  INFO("BLAS\n");
  //matrix_show(&TMP);
  INFO("CUSTOM\n")
    //matrix_show(&C);
#endif //COMPARE
  // ===========================================================================
  INFO("=== Cleanup\n");
  matrix_clear(&A);
  matrix_clear(&B);
  matrix_clear(&C);
}

#include <unistd.h>  // sysconf()
#include <omp.h>
#include "matrix.h"

static void gemm_opt_tile(
  int n_t, double alpha, const double* __restrict__ A, const double* __restrict__ B, double beta,
  double* __restrict__ C, int ldc)
{
  /* #define alignment TILE_SIZE * sizeof(double) */
  /*   double * __restrict__ a = __builtin_assume_aligned(A, alignment); */
  /*   double * __restrict__ b = __builtin_assume_aligned(B, alignment); */
  /*   double * __restrict__ c = __builtin_assume_aligned(C, alignment); */
  // beta*C
  if (beta != 1.0)
  {
    for (int j = 0; j < TILE_SIZE; j++)
    {
#pragma omp simd
      for (int i = 0; i < TILE_SIZE; i++)
      {
        C[e(i, j, ldc)] *= beta;
      }
    }
  }

  double atimesb[TILE_SIZE * TILE_SIZE];
  for (int i = 0; i < TILE_SIZE * TILE_SIZE; i++)
  {
    atimesb[i] = 0;
  }
  // A*B
  for (int j = 0; j < n_t; j++)
  {
    for (int i = 0; i < TILE_SIZE; i++)
    {
      double bb = B[e(i, j, TILE_SIZE)];
#pragma omp simd
      for (int k = 0; k < TILE_SIZE; k++)
      {
        atimesb[e(k, i, TILE_SIZE)] += A[e(k, j, TILE_SIZE)] * bb;
      }
    }
  }

  // C + alpha*A*B
  if (alpha == 1.0)
  {
    for (int j = 0; j < TILE_SIZE; j++)
    {
#pragma omp simd
      for (int i = 0; i < TILE_SIZE; i++)
      {
        C[e(i, j, ldc)] += atimesb[e(i, j, TILE_SIZE)];
      }
    }
  }
  else
  {
    for (int j = 0; j < TILE_SIZE; j++)
    {
#pragma omp simd
      for (int i = 0; i < TILE_SIZE; i++)
      {
        C[e(i, j, ldc)] += alpha * atimesb[e(i, j, TILE_SIZE)];
      }
    }
  }
}

static void copy_tile(int c, const double* __restrict__ src, int ld, double* __restrict__ dest)
{
  for (int m = 0; m < c; m++)
  {
    //#pragma omp simd
    for (int n = 0; n < TILE_SIZE; n++)
    {
      dest[e(n, m, TILE_SIZE)] = src[e(n, m, ld)];
    }
  }
}

static void copy_supertile(
  int m, int n, const double* __restrict__ src, int ld, double* __restrict__ dest)
{
  int m_m = m / TILE_SIZE;
  int m_r = m % TILE_SIZE;
  for (int i = 0; i < m_m; i++)
  {
    copy_tile(n, src + e(0, i, TILE_SIZE), ld, dest + i * n * TILE_SIZE);
  }
  if (m_r > 0)
  {
    for (int j = 0; j < n; j++)
    {
      //#pragma omp simd
      for (int i = 0; i < m_r; i++)
      {
        dest[e(i, j, TILE_SIZE)] = src[e(i, j, ld)];
      }
      //#pragma omp simd
      for (int i = m_r; i < TILE_SIZE; i++)
      {
        dest[e(i, j, TILE_SIZE)] = 0.0;
      }
    }
  }
}

static void copy_tile_trans(
  int r, const double* __restrict__ src, int ld, double* __restrict__ dest)
{
  for (int n = 0; n < r; n++)
  {
    //#pragma omp simd
    for (int m = 0; m < TILE_SIZE; m++)
    {
      dest[e(m, n, TILE_SIZE)] = src[e(n, m, ld)];
    }
  }
}

static void copy_supertile_trans(
  int m, int n, const double* __restrict__ src, int ld, double* __restrict__ dest)
{
  int n_m = n / TILE_SIZE;
  int n_r = n % TILE_SIZE;
  for (int i = 0; i < n_m; i++)
  {
    copy_tile_trans(m, src + e(0, i * TILE_SIZE, ld), ld, dest + i * m * TILE_SIZE);
  }
  if (n_r > 0)
  {
    for (int i = 0; i < m; i++)
    {
      //#pragma omp simd
      for (int j = 0; j < n_r; j++)
      {
        dest[e(j, i, TILE_SIZE)] = src[e(i, j, ld)];
      }
      //#pragma omp simd
      for (int j = n_r; j < TILE_SIZE; j++)
      {
        dest[e(j, i, TILE_SIZE)] = 0.0;
      }
    }
  }
}

static void gemm_opt_supertile(
  int m, int p, int n, double alpha, const double* __restrict__ A, const double* __restrict__ B,
  double beta, double* __restrict__ C, int ldc)
{
  // round up
  int m_m = (m + TILE_SIZE - 1) / TILE_SIZE;
  int p_m = (p + TILE_SIZE - 1) / TILE_SIZE;
  // get remaining
  int m_r = m % TILE_SIZE;
  int p_r = p % TILE_SIZE;
  for (int j = 0; j < p_m; j++)
  {
    int co = (j != p_m - 1 || p_r == 0) ? TILE_SIZE : p_r;
    for (int i = 0; i < m_m; i++)
    {
      int ro = (i != m_m - 1 || m_r == 0) ? TILE_SIZE : m_r;

      if (ro == TILE_SIZE && co == TILE_SIZE)
      {
        gemm_opt_tile(
          n, alpha, A + i * n * TILE_SIZE, B + j * n * TILE_SIZE, beta,
          C + e(i * TILE_SIZE, j * TILE_SIZE, ldc), ldc);
      }
      else
      {
        static double c[TILE_SIZE * TILE_SIZE];

        gemm_opt_tile(n, alpha, A + i * n * TILE_SIZE, B + j * n * TILE_SIZE, 0.0, c, TILE_SIZE);
        if (alpha != 1.0)
        {
          for (int k = 0; k < co; k++)
          {
#pragma omp simd
            for (int l = 0; l < ro; l++)
            {
              C[e(i * TILE_SIZE + l, j * TILE_SIZE + k, ldc)] *= alpha;
            }
          }
        }

        for (int k = 0; k < co; k++)
        {
#pragma omp simd
          for (int l = 0; l < ro; l++)
          {
            C[e(i * TILE_SIZE + l, j * TILE_SIZE + k, ldc)] *= c[e(l, k, TILE_SIZE)];
          }
        }
      }
    }
  }
}

void gemm_opt(
  int m, int p, int n, double alpha, const double* __restrict__ A, int lda,
  const double* __restrict__ B, int ldb, double beta, double* __restrict__ C, int ldc)
{
  double* a = __builtin_assume_aligned(A, MATRIX_ALIGNMENT);
  double* b = __builtin_assume_aligned(B, MATRIX_ALIGNMENT);
  double* c = __builtin_assume_aligned(C, MATRIX_ALIGNMENT);

  int m_m = (m + STILE_SIZE_M - 1) / STILE_SIZE_M;
  int p_m = (p + STILE_SIZE_P - 1) / STILE_SIZE_P;
  int n_m = (n + STILE_SIZE_N - 1) / STILE_SIZE_N;

  int m_r = m % STILE_SIZE_M;
  int p_r = p % STILE_SIZE_P;
  int n_r = n % STILE_SIZE_N;

  static double a_stile[STILE_SIZE_M * STILE_SIZE_N];  //__attribute__ ((aligned));
  static double b_stile[STILE_SIZE_N * STILE_SIZE_P];  //__attribute__ ((aligned));
  /* for (int i = 0; i < STILE_SIZE_M * STILE_SIZE_N; i++) */
  /* { */
  /*   a_stile[i] = 0; */
  /* } */
  /* for (int i = 0; i < STILE_SIZE_P * STILE_SIZE_N; i++) */
  /* { */
  /*   b_stile[i] = 0; */
  /* } */

  for (int j = 0; j < p_m; j++)
  {
    int pk = (j != p_m - 1 || p_r == 0) ? STILE_SIZE_P : p_r;
    for (int k = 0; k < n_m; k++)
    {
      int nk       = (k != n_m - 1 || n_r == 0) ? STILE_SIZE_N : n_r;
      double beta1 = (k == 0) ? beta : 1;
      copy_supertile_trans(nk, pk, b + e(k * STILE_SIZE_N, j * STILE_SIZE_P, ldb), ldb, b_stile);
      for (int i = 0; i < m_m; i++)
      {
        int mk = (i != m_m - 1 || m_r == 0) ? STILE_SIZE_M : m_r;
        copy_supertile(mk, nk, a + e(i * STILE_SIZE_M, k * STILE_SIZE_N, lda), lda, a_stile);
        gemm_opt_supertile(
          mk, pk, nk, alpha, a_stile, b_stile, beta1,
          c + e(i * STILE_SIZE_M, j * STILE_SIZE_P, ldc), ldc);
      }
    }
  }
}

void gemm_blas(
  const int m, const int n, const int k, const double alpha, double* a, const int lda, double* b,
  const int ldb, const double beta, double* c, const int ldc)
{
  char trans = 'N';
  dgemm_(&trans, &trans, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);
}

#include <stdio.h>
#include <assert.h>

typedef struct
{
    char c1;
} s1;
// s1 contains only 1 char, so it should be the same size as one single char
static_assert(sizeof(s1) == sizeof(char), "size of s1 should be equal to the size of one char");

typedef struct
{
    char c1;
    // char padding1[3]
    int i1;
    char c2;
    // char padding2[3]
} s2;
// since int is the biggest datatype in the struct, padding is introduced for the two smaller
// datatypes
static_assert(sizeof(s2) == sizeof(int[3]), "size of s2 should be equal to the size of 3 ints");

typedef struct
{
    char c1;
    char c2;
    // char padding[2]
    int i1;
} s3;
// With the changed order of variables in comparison to s2 less padding is required as you can see
// above.
static_assert(sizeof(s3) == sizeof(int[2]), "size of s3 should be equal to the size of 2 ints");
// Have a look at https://godbolt.org/z/mgotzc to verify that the padding indicated by the
// comments is correct.

int main()
{
    printf("Size of int %lu\n", sizeof(int));
    printf("Size of char %lu\n", sizeof(char));
    printf("Size of s1 %lu\n", sizeof(s1));
    printf("Size of s2 %lu\n", sizeof(s2));
    printf("Size of s3 %lu\n", sizeof(s3));

    s2 b                     = {0, 1, 2};
    s3 c                     = {0, 1, 2};
    unsigned long padding_s2 = (sizeof(int) - (sizeof(char) % sizeof(int)));
    unsigned long d_s2       = (unsigned long) &(b.i1) - (unsigned long) &(b.c1);
    unsigned long d_s2_2     = (unsigned long) &(b.c2) - (unsigned long) &(b.i1);
    printf("For s2: \n");
    printf("distance between c1 and i1: %lu\n", d_s2);
    printf("distance between c1 and i1: %lu\n ", d_s2_2);
    printf("Padding necessary for 4-byte alignment: %lu\n", padding_s2);

    // since the two chars are equivalent to char[2] padding for s3 can be computed as follows
    unsigned long padding_s3 = (sizeof(int) - (sizeof(char[2]) % sizeof(int)));
    unsigned long d_s3       = (unsigned long) &(c.c2) - (unsigned long) &(c.c1);
    unsigned long d_s3_2     = (unsigned long) &(c.i1) - (unsigned long) &(c.c2);
    printf("For s3: \n");
    printf("distance between c1 and c2: %lu\n", d_s3);
    printf("distance between c2 and i1: %lu\n", d_s3_2);
    printf("Padding necessary for 4-byte alignment: %lu\n", padding_s3);
}

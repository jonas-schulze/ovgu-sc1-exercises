#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define F(var) _Generic((var), double : "%lf", float : "%f")

typedef double data_t;
typedef struct
{
    int size;
    int capacity;  // maximum size
    data_t* data;
} stack_tt;

/**
 * @brief stack_clear frees all allocated memory.
 */
void stack_clear(stack_tt* s)
{
    // Freeing `NULL` should be a no-op, but just to be safe:
    if (s != NULL && s->data != NULL)
    {
        free(s->data);
        s->data = NULL;
    }
}

/**
 * @brief stack_init initializes an empty stack with given capacity.
 * @param s the stack to be initialized
 * @param cap the capacity / maximum size to be reserved for s
 * @returns whether successful or not.
 */
bool stack_init(stack_tt* s, int cap)
{
    if (s == NULL)
    {
        return false;
    }
    s->size     = 0;
    s->capacity = cap;
    s->data     = (data_t*) malloc(sizeof(data_t) * cap);
    return s->data != NULL;
}

/**
 * @brief stack_empty checks whether the given stack contains no elements.
 */
bool stack_empty(stack_tt* s)
{
    return s == NULL || s->data == NULL || s->size == 0;
}

/**
 * @brief stack_ttop returns the top element of the stack, if existent.
 * @returns top element or NULL, if empty
 */
data_t* stack_ttop(stack_tt* s)
{
    if (stack_empty(s))
    {
        return NULL;
    }
    // Equivalent to &s->data[s->size-1],
    // but it should produce the same code anyways.
    return s->data + s->size - 1;
}

/**
 * @brief stack_push puts an element onto the stack.
 * @param s the stack to be pushed onto
 * @param e the value to be added
 * @returns whether successful or not
 *
 * Assumes that s is either NULL or has been initialized using stack_init.
 */
bool stack_push(stack_tt* s, data_t e)
{
    if (s == NULL || s->size >= s->capacity)
    {
        return false;
    }
    s->data[s->size] = e;
    s->size += 1;
    return true;
}

/**
 * @brief stack_pop stores the top element of the stack in e.
 * @param s the stack
 * @param e will be set to the top element, if existent
 * @returns whether a value has been stored in e.
 */
bool stack_pop(stack_tt* s, data_t* e)
{
    data_t* top = stack_ttop(s);
    if (top == NULL)
    {
        return false;
    }
    *e = *top;
    s->size -= 1;
    return true;
}

/**
 * @brief vector_push puts an element onto the stack.
 * @param s the stack to be pushed onto
 * @param e the value to be added
 * @returns whether successful or not
 *
 * Assumes that s is either NULL or has been initialized using stack_init.
 *
 * Grows stack if needed.
 */
bool vector_push(stack_tt* s, data_t e)
{
    if (s == NULL)
    {
        return false;
    }
    bool ok = stack_push(s, e);
    if (ok)
    {
        return true;
    }
    // We need to grow the stack.
    int new_capacity = 1 + 1.5 * s->capacity;
    stack_tt new_s;
    ok = stack_init(&new_s, new_capacity);
    if (!ok)
    {
        return false;
    }
    memcpy(new_s.data, s->data, s->size * sizeof(data_t));
    new_s.size = s->size;
    stack_clear(s);
    *s = new_s;
    return stack_push(s, e);
}

int main()
{
    bool ok;
    stack_tt s;
    ok = stack_init(&s, 1);
    if (!ok)
    {
        fprintf(stderr, "Could not allocate memory for stack.\n");
        return EXIT_FAILURE;
    }

    // Start reading values:
    data_t value;
    printf("Gimme some positive numbers:\n");
    scanf(F(value), &value);
    while (value > 0)
    {
        ok = vector_push(&s, value);
        if (!ok)
        {
            fprintf(stderr, "Could not push element to stack.\n");
            return EXIT_FAILURE;
        }
        scanf(F(value), &value);
    }
    fprintf(stderr, "Ignoring last value, ");
    fprintf(stderr, F(value), value);
    fprintf(stderr, ", as it wasn't positive.\n");

    // Start printing values:
    printf("Here are your numbers in reverse:\n");
    if (stack_empty(&s))
    {
        fprintf(stderr, "No positive numbers have been entered.\n");
        return EXIT_SUCCESS;
    }
    while (stack_pop(&s, &value))
    {
        printf(F(value), value);
        printf("\n");
    }
    return EXIT_SUCCESS;
}

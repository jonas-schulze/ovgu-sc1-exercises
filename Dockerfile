FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    build-essential \
    git \
    vim \
    gfortran \
    libopenblas-dev \
    liblapack-dev \
    gdb \
    valgrind

COPY . /sc1/
WORKDIR /sc1/

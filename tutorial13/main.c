#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "my_matrix.h"

void sparse_mvp(struct sparse_matrix_st* A, double* x, double* y)
{
}

int main(int argc, char** argv)
{
    struct sparse_matrix_st A;
    double tic, toc;
    double *x, *y;
    int i, j;
    double flops;

    /*-----------------------------------------------------------------------------
     * read matrix
     *-----------------------------------------------------------------------------*/
    sparse_matrix_read("A625.mtx", &A);
    // sparse_matrix_print(&A);

    /* ....  */

    sparse_matrix_clear(&A);
    return 0;
}

#ifndef MY_MATRIX_H
#define MY_MATRIX_H

#ifndef INT
#define INT int
#endif

struct my_matrix_st
{
    INT cols;
    INT rows;
    INT LD;
    double* values;
    char structure;
};

int my_matrix_read(char* filename, struct my_matrix_st* A);
int my_matrix_rand(struct my_matrix_st* A, INT rows, INT cols);
void my_matrix_print(struct my_matrix_st* A);
void my_matrix_clear(struct my_matrix_st* A);

struct sparse_matrix_st
{
    INT cols;
    INT rows;
    INT nnz;
    INT* rowptr;
    INT* colptr;
    double* values;
};

void sparse_matrix_clear(struct sparse_matrix_st* mat);
void sparse_matrix_print(struct sparse_matrix_st* mat);
int sparse_matrix_read(char* filename, struct sparse_matrix_st* S);

double wtime();

#endif

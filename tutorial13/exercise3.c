#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_matrix.h"

/**
 * sparse_mvp computes y <- Ax
 */
void sparse_mvp(struct sparse_matrix_st* A, double* x, double* y)
{
    int i, j;
    for (i = 0; i < A->cols; i++)
    {
        y[i] = 0;
        for (j = A->rowptr[i]; j < A->rowptr[i + 1]; j++)
        {
            y[i] += A->values[j] * x[A->colptr[j]];
        }
    }
}

double dnrm2_(int* n, double* x, int* incx);
void daxpy_(int* n, double* alpha, double* x, int* incx, double* y, int* incy);
double ddot_(int* n, double* x, int* incx, double* y, int* incy);
void dscal_(int* n, double* alpha, double* x, int* incx);

/**
 * cg performs the conjugate gradient method for solving Ax = b, starting at
 * x0 = x. The implementation is according to Algorithm 8.1 from the lecture.
 *
 * @returns 0 on success
 * @returns 1 if not convergent
 */
int cg(struct sparse_matrix_st* A, double* x, double* b, int maxit, double tol)
{
    int i;
    static double one     = 1;
    static double neg_one = -1;
    static int inc        = 1;

    int n       = A->cols;
    size_t size = A->cols * sizeof(double);

    // Initialize r0:
    double* r = malloc(size);
    sparse_mvp(A, x, r);
    daxpy_(&n, &neg_one, b, &inc, r, &inc);
    dscal_(&n, &neg_one, r, &inc);

    // Initialize p0:
    double* p = malloc(size);
    memcpy(p, r, size);

    // Initialize alpha0:
    double alpha = dnrm2_(&n, r, &inc);
    alpha *= alpha;

    double* v = malloc(size);
    double lambda;
    for (i = 0; i < maxit; i++)
    {
        if (alpha < tol)
            return 0;

        printf("it %2d: alpha=%g\n", i, alpha);

        sparse_mvp(A, p, v);
        lambda = alpha / ddot_(&n, v, &inc, p, &inc);
        daxpy_(&n, &lambda, p, &inc, x, &inc);
        lambda *= -1;
        daxpy_(&n, &lambda, v, &inc, r, &inc);
        lambda = dnrm2_(&n, r, &inc);
        lambda *= lambda;  // alpha_{m+1}
        alpha = lambda / alpha;
        dscal_(&n, &alpha, p, &inc);
        daxpy_(&n, &one, r, &inc, p, &inc);
        alpha = lambda;
    }

    return 1;
}

int main(int argc, char** argv)
{
    struct sparse_matrix_st A;
    double *x, *y;
    int i;

    sparse_matrix_read("A10000.mtx", &A);

    x = malloc(sizeof(double) * A.cols);
    y = malloc(sizeof(double) * A.cols);
    for (i = 0; i < A.cols; i++)
        x[i] = 1;
    sparse_mvp(&A, x, y);  // y <- Ax = b
    for (i = 0; i < A.cols; i++)
        x[i] = 0;

    int status = cg(&A, x, y, 1000, 1e-8);
    printf("status: %d\n", status);

    double err = 0;
    for (i = 0; i < A.cols; i++)
    {
        err += (x[i] - 1) * (x[i] - 1);
    }
    printf("err: %e\n", err);

    sparse_matrix_clear(&A);
    return 0;
}

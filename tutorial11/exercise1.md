# Exercise 1

We know that $\kappa_2(A) = \frac{\sigma_1}{\sigma_n}$ with $\sigma_1$ being the largest
singular value and $\sigma_n$ the smallest one. Since $AA^H = I$ if $A$ is unitary, we get
$\sigma_\cdot = 1$ and $\sigma_1 = \sigma_n = \pm 1$, which concludes the proof.

These properties can be used to design numerically stable algorithms, for example in the
QR-factorization.

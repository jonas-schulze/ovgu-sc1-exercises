#include <stdio.h>

void dgeev_(
  const char* JOBVL, const char* JOBVR, const int* N, const double* A, const int* LDA, double* WR,
  double* WI, double* VL, const int* LDVL, double* VR, const int* LDVR, double* WORK,
  const int* LWORK, int* INFO);

void print_ev_pair(double wr, double wi, double* vl, double* vr)
{
    printf("-----");
    printf("w = %g + %gi\n", wr, wi);
    printf("vl =");
    for(int i = 0; i < 4; i++) {
        printf("  %g", vl[i]);
    }
    printf("\n");
    printf("vr =");
    for(int i = 0; i < 4; i++) {
        printf("  %g", vr[i]);
    }
    printf("\n");
}

int main()
{
    const int LD = 4;
    const int N = 4;
    // Eigenvalues, real and imaginary parts:
    double WR[4], WI[4];
    // Left and right eigenvectors:
    double VL[16], VR[16];
    // Internal work space:
    int LWORK = 64;  // >= 4N
    double WORK[64];
    int INFO;

    // clang-format off
    double A[16] = {
      3,  6,  2,  1,
      8, -4,  5,  5,
      2,  1, -8, -8,
      1,  5,  8, -7
    };
    // clang-format on

    dgeev_("V", "V", &N, A, &LD, WR, WI, VL, &LD, VR, &LD, WORK, &LWORK, &INFO);

    printf("status: %d\n", INFO);
    for(int i = 0; i<N;i++){
      print_ev_pair(WR[i], WI[i], VL+i*LD, VR+i*LD);
    }
}

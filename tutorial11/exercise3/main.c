#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "my_matrix.h"

/* Here must be de declaration of the LAPACK functions you need.   */


/*-----------------------------------------------------------------------------
 *  The my_matrix_st structure collects all information about a matrix, like 
 *  number of rows, number of columns and the leading dimension together with 
 *  its values. It is defined as follows:
 *
 *  struct my_matrix_st {
 *  	int cols; 
 *  	int rows; 
 *  	int LD; 
 *  	double * values; 
 *  	char structure; 
 *  }; 

 *-----------------------------------------------------------------------------*/

int main (int argc , char *argv ){
	struct my_matrix_st A; 
	struct my_matrix_st RHS1, RHS2; 
	double tic,toc; 

	// read a dense matrix to A and measure the time
	tic = wtime(); 
	/*  Read the Matrix  */
	if ( my_matrix_read ("matrix.mtx", &A)){
		fprintf(stderr,"Can not read matrix.\n"); 
		return 1; 
	}
	/*  Read the first right hand side  */
	if ( my_matrix_read ("rhs1.mtx", &RHS1)){
		fprintf(stderr,"Can not read the first right hand side.\n"); 
		return 1; 
	}

	/*  Read the second right hand side  */
	if ( my_matrix_read ("rhs2.mtx", &RHS2)){
		fprintf(stderr,"Can not read the second right hand side.\n"); 
		return 1; 
	}

	toc = wtime(); 
	printf("Reading the matrix took %lg seconds\n", toc - tic);



	/*-----------------------------------------------------------------------------
	 *  Here begin your code. Solve A x = RHS1 and A y = RHS2 and write both 
	 *  soltion x and y to a file name "solution.dat" which has to look like: 
	 *
	 *   x_1 y_1 
 	 *   x_2 y_2 
	 *   ....
	 *   ...
	 *   x_n y_n 
	 *-----------------------------------------------------------------------------*/

	/* Replace me by your soultion :-)   */


	
	
	
	
	/* Do not change anything below */
	
	/* This line can be commeted out during development, but for the final result it is necessary.  */
	system("gnuplot ./display.plot"); 

	
	my_matrix_clear(&A); 
	my_matrix_clear(&RHS1); 
	my_matrix_clear(&RHS2); 
	return 0; 

}

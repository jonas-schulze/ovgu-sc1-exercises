# Exercise 4

We claim that the recursion formula is the following and prove it by induction.
\begin{equation*}
    f_1 = 0,\;f_n = n\cdot f_{n-1} + n + (n-1)
\end{equation*}

#### Case $n=2$

$f_2 = 2\cdot 0 + 2 + 1$ which is true since $\displaystyle \begin{vmatrix}a & b\\ c & d\end{vmatrix} = ad + -1 \cdot bc$.

#### Assuming that this holds for arbitrary $n$, show that it holds for $n + 1$

Looking at the recursion formula we see that we get a sum of $n$ cofactors,
justifying the first summand in the recursion formula.
The second summand resolves the multiplications with $a_{ik}$,
of which there is one for each cofactor.
The final summand resolves the alternating sum of the cofactors.

#### Computing a $100-\times-100$ determinant

Approximate computation time of the determinant of $A \in \mathbb{R}^{n\times n}$
with some simple `julia` code:

```julia
f(n::BigInt) = (n == 1) ? 0 : (n * f(n - 1) + n + n-1)
f(BigInt(100))/(300*1e9)
```

    8.456231852004243247175691607076007348171585952209938074425839532914975130279497e+146

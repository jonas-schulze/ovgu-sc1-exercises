
# Exercise 2

#### Compute the solution of the linear system


```julia
A = [2 -1 1; -1 1e-10 1e-10; 1 1e-10 1e-10]
b = [2*(1+1e-10); -1e-10; 1e-10]
x = A\b
```




    3-element Array{Float64,1}:
      1.000000082740371e-10
     -0.9999999793149086   
      1.0000000206850914   



#### Check that $\kappa_\infty(A) = 2\cdot 10^{10}$


```julia
using LinearAlgebra
opnorm(A, Inf)*opnorm(inv(A), Inf)
```




    1.9999998756894512e10



#### Check that $\kappa_\infty(DAD) \leq 5$


```julia
D = Diagonal([1e-5; 1e5; 1e5])
```




    3×3 Diagonal{Float64,Array{Float64,1}}:
     1.0e-5        ⋅         ⋅ 
      ⋅      100000.0        ⋅ 
      ⋅            ⋅   100000.0




```julia
opnorm(D*A*D, Inf)*opnorm(inv(D*A*D), Inf)
```




    3.0



# Exercise 1

Show that $x^\top y$ is an inner product:

* _Symmetry_ $x^\top y =\sum_{i=1}^n x_i y_i = \sum_{i=1}^n y_i x_i = y^\top x$
* _Linearity_

  * $\langle \alpha x,\, y\rangle = \sum_{i=1}^n \alpha x_i y_i = \alpha \sum_{i=1}^n x_i y_i = \alpha \langle x,\, y\rangle$
  * $\langle x,\, y + z\rangle = \sum x_i (y_i + z_i) = \sum_{i=1}^n x_i y_i + \sum_{i=1}^n x_i z_i$

* _Positive definite_
  * $\langle x,\, x\rangle = \sum_{i=1}^n x_i^2 \geq 0$, since $x_i^2 \geq 0$
  * $\langle x,\, x\rangle = 0 \Longleftrightarrow x=0$, $\Longleftarrow$ is obvious. The other direction holds, since
     the summands are greater or equal zero. In order to get a sum of value zero, every summand has to be zero.

We can easily see from the properties above that $\sqrt{\langle x,\, x\rangle}$ is a norm and the only interesting thing
to proof is the triangle inequality.
Using the Cauchy-Schwarz inequality we get:
\begin{align*}
    \lVert x+y \rVert^2 &= \langle x+y,\, x+y\rangle
    = \langle x,\, x\rangle + 2 \langle x,\,y \rangle + \langle y,\,y \rangle\\
    &\leq \lVert x \rVert^2 + 2\lVert x\rVert  \lVert y\rVert + \lVert y\rVert^2
    =(\lVert x \rVert +\lVert y\rVert)^2,
\end{align*}
which concludes the proof.


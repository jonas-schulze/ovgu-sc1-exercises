---
title: Exercise 4
author:
- Jonas Schulze
- Nils Margenberg
---

\newcommand\R{\mathbb{R}}
\renewcommand\C{\mathbb{C}}

# Part a

Let $Q,\tilde{Q} \in \C^{n,n}$ be unitary and $I_n \in \R^{n,n}$ the identity,
i.e. $XX^H = I_n$ for $X=Q,\tilde{Q}$, where $\cdot^H$ denotes the conjugate
transpose matrix. Then
$$
  (Q\tilde{Q})(Q\tilde{Q})^H =
  Q\underbrace{\tilde{Q}\tilde{Q}^H}_{I_n}Q^H =
  QQ^H =
  I_n
$$
that means $Q\tilde{Q}$ is unitary as well.

# Part b

Let $e_i \in \R^n$ denote the canonical unit vector and let $E_{ij} := e_ie_j^T$
be the matrix having a one $(i,j)$ and zeros elsewhere, then:
\begin{align*}
  G(i,j,\theta) &= I_n + (c-1)(E_{ii} + E_{jj}) + sE_{ij} - sE_{ji} =: G \\
  G(i,j,\theta)^H &= I_n + (c-1)(E_{ii} + E_{jj}) - sE_{ij} + sE_{ji} = G^H
\end{align*}
Using $i \neq j$ and
$$
  E_{ij}E_{kl} = \begin{cases}
    E_{il} & j=k \\
    0_{n,n} & j \neq k
  \end{cases}
$$
it follows that
\begin{align*}
  GG^H
  &= I_n \begin{aligned}[t]
    &+ (c-1)(E_{ii}+E_{jj}) + sE_{ij} - sE_{ji} \\
    &+ (c-1)(E_{ii}+E_{jj}) + (c-1)^2(E_{ii} + E_{jj}) + s(c-1)E_{ij} - s(c-1)E_{ji} \\
    &- sE_{ij} - (c-1)sE_{ij} + (-s)^2E_{jj} \\
    &+ sE_{ji} + (c-1)sE_{ji} + s^2E_{ii} \\
  \end{aligned} \\
  &= I_n \begin{aligned}[t]
    &+ \big( (c-1)^2 + 2(c-1) + s^2 \big)(E_{ii}+E_{jj}) \\
    &+ \big( s + s(c-1) - s - (c-1)s \big)E_{ij} \\
    &+ \big( -s - s(c-1) + s + (c-1)s \big)E_{ij} \\
  \end{aligned} \\
  &= I_n + (s^2+c^2-1)(E_{ii}+E_{jj}) = I_n.
\end{align*}
Assuming $i=j$ makes no sense in terms of a rotation in the $(i,j)$-plane.

---
title: Exercise 6
author:
- Jonas Schulze
- Nils Margenberg
header-includes: \usepackage{etoolbox}
---

\newcommand\LandauO{\mathcal{O}}
\newcommand\optional[1]{\ifblank{#1}{\,\cdot\,}{#1}}
\newcommand\norm[1]{\lVert \optional{#1} \rVert}
\newcommand\normF[1]{\norm{#1}_F}
\newcommand\normi[1]{\norm{#1}_1}
\newcommand\normii[1]{\norm{#1}_2}
\newcommand\norminf[1]{\norm{#1}_\infty}

Computing $A^HA$ needs $\LandauO(n^3)$ operations, computing its biggest Eigenvalue
needs another $\LandauO(n^3)$, causing bad complexity and big coefficients.
Computing $\normii{}$ using its definition $\max_{\normii{x}=1} \ldots$ is
just as expensive, if not more expensive.

In contrast, $\normF{}$, $\normi{}$ and $\norminf{}$ all need $\LandauO(n^2)$
operations. By the equivalence of all matrix norms (not only the induced ones)
and by knowing the corresponding constants (see Exercise 7), it is always
possible to exchange $\normii{}$ for one of the cheaper ones mentioned above.

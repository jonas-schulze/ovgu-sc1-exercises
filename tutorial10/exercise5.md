# Exercise 5

* $\max \frac{\lVert Ix\rVert}{\lVert x\rVert} = \frac{\lVert x\rVert}{\lVert x\rVert}=1$
* One way to see that the Frobenius norm is not induced is to look at the norm of the
  identity matrix:
  $\lVert I \rVert_F = \sqrt{\sum_{i=1}^n\sum_{j=1}^n \delta_{ij}}=\sqrt{n}\neq 1$
  
  
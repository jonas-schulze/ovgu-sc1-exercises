# Exercise 7

To show the inequalities we use the following (with $x\in\mathbb{R}^n$):

1. $\lvert x_k\rvert^2\leq \sum_{j=1}^n \lvert x_j\rvert^2$

2. $\sum_{i=1}^n x_i^2\leq \left( \sum_{i=1}^n \lvert x_i \rvert\right)^2$

3. $\sum_{j=1}^n x_j^2 \leq n \max \lvert x_i \rvert^2 = n (\max \lvert x_i\rvert)^2$

4. Using Cauchy-Schwarz inequality:
   $\lVert x\rVert_1=\sum_{i=1}^n 1 \lvert x_i \rvert \leq \sqrt{\left(\sum_{i=1}^n 1\right)}
  \sqrt{\left(\sum_{i=1}^n \lvert x_i \rvert^2\right)}=\sqrt n \lVert x\rVert_2$ 
5. $\sum_{j=1}^n \lvert x_j\rvert \leq n \max \lvert x_i \rvert$

6. $\lvert x_k\rvert\leq \sum_{j=1}^n \lvert x_j\rvert$

The first inequality follows directly from 2 and 4, the second from 1 and 3 setting $\lvert x_k\rvert = \max \lvert x_i \rvert$.
The third inequality follow from 5 and 6.

Of course the inequalities above hold setting $x = Ay$.  
1. Now using the a) we get:
   \begin{equation*}
    \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
    \norm{2}{A}
    = \max_{x \neq 0} \frac{\norm{2}{Ax}}{\norm{2}{x}}
    \leq \max_{x \neq 0} \frac{\norm{1}{Ax}}{\norm{1}{x}/\sqrt{n}}
    = \sqrt{n}\norm{1}{A}
   \end{equation*}
   \begin{equation*}
   \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
   \norm{1}{A}
    = \max_{x \neq 0} \frac{\norm{1}{Ax}}{\norm{1}{x}}
    \leq \max_{x \neq 0} \frac{\sqrt{n}\norm{2}{Ax}}{\norm{2}{x}}
    = \sqrt{n}\norm{2}{A}
   \end{equation*}
2. Using b) we get:
   \begin{equation*}
    \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
    \norm{\infty}{A}
    = \max_{x \neq 0} \frac{\norm{\infty}{Ax}}{\norm{\infty}{x}}
    \leq \max_{x \neq 0} \frac{\norm{2}{Ax}}{\norm{2}{x}/\sqrt{n}}
    = \sqrt{n}\norm{2}{A}
   \end{equation*}
   \begin{equation*}
   \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
    \norm{2}{A}
    = \max_{x \neq 0} \frac{\norm{2}{Ax}}{\norm{2}{x}}
    \leq \max_{x \neq 0} \frac{\sqrt{n}\norm{\infty}{Ax}}{\norm{\infty}{x}}
    = \sqrt{n}\norm{\infty}{A}
   \end{equation*}
3. Using c)
   \begin{equation*}
    \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
    \norm{1}{A}
    = \max_{x \neq 0} \frac{\norm{1}{Ax}}{\norm{1}{x}}
    \leq \max_{x \neq 0} \frac{n\norm{\infty}{Ax}}{\norm{\infty}{x}/\sqrt{n}}
    = n\norm{\infty}{A}
   \end{equation*}
   \begin{equation*}
    \newcommand\norm[2]{\lVert #2 \rVert_{#1}}
    \norm{\infty}{A}
    = \max_{x \neq 0} \frac{\norm{\infty}{Ax}}{\norm{\infty}{x}}
    \leq \max_{x \neq 0} \frac{\norm{1}{Ax}}{\norm{1}{x}\frac{1}{n}}
    = n\norm{1}{A}
   \end{equation*}

We see that the norm is bounded by recognizing
that $\max_{x\neq 0} \frac{\lVert Ax \rVert}{\lVert x\rVert} = \max_{\lVert x\rVert = 1} \lVert Ax \rVert$,
independent of the norm: Using the properties of a norm we have $\lVert Ax \rVert = \lVert \lVert x \rVert A\frac{x}{\lVert x \rVert} \rVert =
\lVert x\rVert \lVert A\frac{x}{\lVert x \rVert} \rVert$.
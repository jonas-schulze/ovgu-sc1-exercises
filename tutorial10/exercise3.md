# Exercise 3

__Show that the lower unitriangular matrices are a group.__ Obviously all matrices form a
group, so we will show that the unitriangular matrices $T$ form a subgroup.

* Closure. Let $A,\:B\in T$ unitriangular matrices. The entries of $C=AB$ can be calculated by 
  $c_{ij}=\sum_{k=1}^n a_{ik}b_{kj}$. For all $c_{ij},\:i>j$ it holds $k > j$
  ($\implies b_{kj}=0$) or $i > k$ ($\implies a_{ik}=0$) or both.
  Either way, $c_{ij} = 0 + \ldots + 0$.
  For the diagonal elements if $k>i:\: b_{ki}=0$ and $k<i:\: a_{ik}=0$.
  So $c_{ii} = a_{ii}b_{ii} = 1$.
* The identity matrix is obviously a unitriangular matrix
* $A\in T \implies A^{-1}\in T$. $A$ can be decomposed in $A = I+N$ with
  $N$ a nilpotent matrix and $I$ the identity matrix.
  Using $N^n = 0$ and interpreting $(I+N)^{-1}$ as the limit of a geometric series we get:
  \begin{equation*}
    (I+N)^{-1}=(I-(-N))^{-1}=\sum_{i=0}^n (-N)^i = I + \sum_{i=1}^n (-N)^i \in T
  \end{equation*}
  
  
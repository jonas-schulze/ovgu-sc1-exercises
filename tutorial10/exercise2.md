---
title: Exercise 2
author:
- Jonas Schulze
- Nils Margenberg
---

\newcommand\vspan{\mathop{\operatorname{span}}}
\newcommand\rank{\mathop{\operatorname{rank}}}

Assuming $\exists x : Ax=b$, then $b \in \vspan(A)$ since $x = (x_1,\ldots,x_n)$
is a (not necessarily unique) representation of $b$ with respect to the ordered
spanning set $(a_1, \ldots,a_n) = A$. Hence, $\vspan(A) = \vspan(A,b)$ and
$\rank(A) = \rank(A,b)$.

Vice versa, assuming $\rank(A) = \rank(A,b)$ and by the equality of column and
row rank, $b$ does not increase the column rank of $A$, i.e. $b \in \vspan(A)$
and $\exists x : Ax=b$ similar to above.

As mentioned, $\rank(A) = \rank(A,b)$ does not guarantee uniqueness: suppose
$A = (a_1,\ldots,a_n)$ satisfies $a_1=a_2$, then $b := a_1+a_3$ has at least two
solutions $x : Ax=b$, namely $x=e_1+e_3$ and $x=e_2+e_3$. $e_i$ denotes the
canonical unit vector, i.e. the $i$-th column of the identity matrix.

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>

double ddot_(const int* n, const double* x, const int* incx, const double* y, const int* incy);

int main()
{
    const int n   = 3;
    const int inc = 1;
    double x[3]   = {1, 2, 3};
    double y[3]   = {1, 2, 3};
    double result = ddot_(&n, x, &inc, y, &inc);
    // Check whether the error is within machine precision:
    assert(fabs(result / 14 - 1) < DBL_EPSILON && "DDOT is not accurate");
    printf("%lf\n", result);
}

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>

/**
@brief max_array_sum computes the maximal sum of consecutive elements of a given array.
@param in input array
@param sz size of in
@param first (return value) first index of optimal range
@param last (return value) last index of optimal range, i.e. end-1
@param max_sum (return value) sum of elements in optimal range

KNOWN BUGS
* There is no checks for integer overflows in either direction.
*/
void max_array_sum(int const in[], int sz, int* const first, int* const last, int* const max_sum)
{
    int tmp_max = 0, tmp_idx = 0;  // temporary value for iterating over the array
    *first   = 0;                  // first index of maximum sum subarray
    *last    = 0;                  // last index of maximum sum subarray
    *max_sum = in[0];              // set maximum to lowest integer value
    for (int i = 0; i < sz; i++)
    {
        tmp_max += in[i];  // add next array element...
        if (tmp_max < 0)   // if tmp_max gets negative we can discard what we have so far
        {
            tmp_max = 0;
            if (in[i] > *max_sum)
            {
                *max_sum = in[i];
                *first   = i;
                *last    = i;
            }
            tmp_idx = i + 1;  // prepare setting first position in subarray to next position
        }
        else if (*max_sum < tmp_max)  // did we get better?
        {
            *max_sum = tmp_max;
            *first   = tmp_idx;  // update first position in subarray to non-negative position
            *last    = i;        // set last position in subarray to current position
        }
    }
}

void print_max_array_sum(int const in[], int sz, bool print_sum)
{
    int first = 0, last = INT_MIN;
    int max_sum = 0;
    max_array_sum(in, sz, &first, &last, &max_sum);
    printf("First index: %i, last index: %i\n", first, last);
    if (print_sum)
    {
        for (int i = first; i <= last; i++)
        {
            if (i == first)
            {
                printf("%i ", in[i]);
            }
            else
            {
                printf("+ %i ", in[i]);
            }
            if (i % 10 == 0)
            {
                printf("\n");
            }
        }
        printf("= %i\n", max_sum);
    }
    else
    {
        printf("Max sum: %i\n", max_sum);
    }
}

bool read_array(char const dir[], int** out, int* sz)
{
    FILE* file;
    file = fopen(dir, "r");
    if (!file)
    {
        return false;
    }
    fscanf(file, "%d", sz);
    *out = (int*) malloc(*sz * sizeof(int));
    if (!*out)
    {
        fclose(file);
        return false;
    }
    for (int i = 0; i < *sz; i++)
    {
        fscanf(file, "%d", &(*out)[i]);
    }
    fclose(file);
    return true;
}

int main()
{
    int my_array[10] = {-1, 3, 4, -2, 5, 1, -9, 4, 2, -2};
    print_max_array_sum(my_array, 10, true);

    int *arr, size;
    bool read_success;
    char d_str[255] = "../tmp/sample_data";
    char path[511];
    DIR* dir;
    struct dirent* d;
    dir = opendir(d_str);
    if (dir)
    {
        // Iterate over the files in `dir`:
        while ((d = readdir(dir)) != NULL)
        {
            // Skip entries "." and "..":
            if (strcmp(d->d_name, ".") != 0 && strcmp(d->d_name, "..") != 0)
            {
                printf("\n");
                sprintf(path, "%s/%s", d_str, d->d_name);
                printf("Filename: %s\n", d->d_name);
                read_success = read_array(path, &arr, &size);
                assert(read_success == true && "Reading file or memory allocation failed");
                print_max_array_sum(arr, size, false);
            }
        }
        closedir(dir);
    }
    return 0;
}

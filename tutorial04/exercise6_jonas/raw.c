
#include <stdio.h>  /* I/O */
#include <memory.h> /* memset() */
#include <math.h>   /* floor(), sqrt() */
#include <stdlib.h> /* malloc() */
#include <assert.h> /* assert() */

int main( int argc, char** argv )
{
    /* === Initialization === */
    int n = 0;
    int factorBound = 0;
    int maxUsedIndex = 0;
    int ** factors = 0;
    int i = 0; /* index */

    (void)argc; /* Unused variables */
    (void)argv;

    while( n <= 1 )
    {
        printf( "Insert a positive number ( >1 ): " );
        scanf( "%d", &n );

        if( n <= 1 )
        {
            printf( "Wrong input. Try again - " );
        }
    }

    /* === Allocation === */
    /* There won't be more than log(n) prime factors, actually the
     * bound is much tighter, but this should be enough */
    factorBound = ceil( log( n ) );
    factors = (int **)malloc( ( factorBound ) * sizeof( int* ) );
    printf( "\n%d\n", factorBound );

    if( !factors )
    {
        printf( "Memory allocation failed. Aborting.\n" );
        return 1;
    }
    for( i = 0; i < factorBound; ++i )
    {
        factors[i] = (int *)malloc( ( 2 * sizeof( int ) ) );
        /* This most likely won't happen, thus an assertion is enough */
        assert( factors[i] && "Memory allocation failure");

        factors[i][0] = -1; /* Number i.e. prime factor */
        factors[i][1] = 0;  /* Multiplicity */
    }

    /* === Factorization === */
    /* start with 2 */
    i = 2;
    while( i <= n )
    {
        /* Test if n is divisible by i */
        if( n % i == 0 )
        {
            // if any matching is found
            char found = 0;
            /* dividible by i search matching index to increase,
             * then start over from 2 */
            for( int k = 0; k <= maxUsedIndex; ++k )
            {
                if( factors[k][0] == i )
                {
                    factors[k][1]++;
                    found = 1;
                    break;
                }
            }
            if( found != 1 )
            {
                assert( maxUsedIndex < factorBound );
                maxUsedIndex++;
                factors[maxUsedIndex][0] = i; /* New factor i */
                factors[maxUsedIndex][1]++;
            }
            n /= i;
            i = 2;
        }
        else
        {
            ++i;
        }
    }

    /* === Print result === */
    printf( "The factorization is: \n   " );
    char start = 0; /* set when the first number was written, to make it beautiful */
    for( i = 0; i < factorBound; ++i )
    {
        int fac = factors[i][0];
        int mul = factors[i][1];
        if( fac != -1 )
        {
            if( start == 1 )
            {
                printf( " * " );
            }
            printf( "%d", fac );
            if( mul > 1 )
            {
                printf( "^%d", mul );
            }
            start = 1;
        }
    }
    printf( "\n" );

    /* === Cleanup === */
    for( i = 0; i < factorBound; ++i )
        free( factors[i] );
    free( factors );
    return 0;
}

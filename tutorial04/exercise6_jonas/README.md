# Comments

1. If you don't intent to use them, do not use them.
2. Use contiguous memory; be more verbose on pointers (e.g., use `NULL`).
3. Fix off-by-1 error.
4. Separate concerns.
5. Use consistent names: `maxUsedIndex` is technical, `factorBound` is mathematical.

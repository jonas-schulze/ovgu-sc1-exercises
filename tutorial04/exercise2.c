#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

// Now we can switch between different types with a compiler flag
#ifdef REALTYPE
typedef REALTYPE realtype;
#else
typedef double realtype;
#endif

// clang-format off
// selecting format specifiers dependent on the type, requires c11 standard
#define F(X) _Generic((X),        \
              long double: "%Lf", \
                    float: "%f",  \
                  default: "%lf"  \
)
// clang-format on
#define println(x) printf(F(x), x), printf("\n");

// struct containing 3 scalars of type `realtype`, which can be set by a compiler flag
// -DREALTYPE=..., default is double.
typedef struct
{
    realtype x;
    realtype y;
    realtype z;
} vec3d;
// Let's check if our struct is as big as a double[3] array, which is what we expect
static_assert(sizeof(vec3d) == sizeof(double[3]), "Padding");

// Print struct vec3d as row vector
void print_vec3d(vec3d* const v)
{
    printf("(");
    printf(F(v->x), v->x), printf(", ");
    printf(F(v->y), v->y), printf(", ");
    printf(F(v->z), v->z);
    printf(")\n");
}

// Allocates memory for vec3d struct and reads 3 scalars from stdin and set them for the struct.
// Returns pointer to vec3d struct.
vec3d* read_vec3d()
{
    vec3d* v = (vec3d*) malloc(sizeof(vec3d));
    printf("Set values of vector.\nEnter x-component: ");
    scanf(F(v->x), &(v->x));
    printf("Enter y-component: ");
    scanf(F(v->x), &(v->y));
    printf("Enter z-component: ");
    scanf(F(v->x), &(v->z));
    return v;
}

// Reads 3 scalars from stdin and sets them in the passed vec3d struct.
void set_vec3d(vec3d* const v)
{
    printf("Set values of vector.\nEnter x-component: ");
    scanf(F(v->x), &(v->x));
    printf("Enter y-component: ");
    scanf(F(v->y), &(v->y));
    printf("Enter z-component: ");
    scanf(F(v->z), &(v->z));
}

// Calculates euclidean distance between two 3d vectors represented by vec3d structs
realtype dist(vec3d* const u, vec3d* const v)
{
    // clang-format off
    return sqrt((u->x - v->x) * (u->x - v->x)
              + (u->y - v->y) * (u->y - v->y)
              + (u->z - v->z) * (u->z - v->z));
    // clang-format on
}

int main(int argc, char const* argv[])
{
    vec3d a;
    set_vec3d(&a);
    printf("Read the vector:\n");
    print_vec3d(&a);
    vec3d* b = read_vec3d();
    printf("Read the vector:\n");
    print_vec3d(b);
    printf("Distance between the two vectors:\n");
    println(dist(&a, b));
    free(b);
    return 0;
}

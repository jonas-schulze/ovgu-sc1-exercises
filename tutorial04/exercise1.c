#include <stdio.h>
#include <stdlib.h>

// gcd returns the greatest common divisor. Performs Euclid's algorithm on the
// absolute values of a and b. Guarantees non-negative output.
int gcd(int a, int b)
{
    // If there is a negative common divisor, its absolute value is a common
    // divisor as well. We only care for the bigger one, that is the positive one.
    a = abs(a);
    b = abs(b);
    // Ensure that a >= b. This property will be preserved throughout.
    if (a < b)
    {
        int tmp = b;
        b       = a;
        a       = tmp;
    }
    // As long as the current reminder is not zero, i.e. positive, let a become
    // the current remainder and compute the new remainder for b.
    while (b != 0)
    {
        int tmp = b;
        b       = a % b;
        a       = tmp;
    }
    return a;
}

// lcm returns the least (but positive) common multiple. If the greatest common
// divisor is non-negative, this means:
//
// a * b == sign(a*b) * lcm(a,b) * gcd(a,b)
int lcm(int a, int b)
{
    // Edge case:
    if (a == 0 || b == 0)
    {
        return 0;
    }
    return abs(a * b) / gcd(a, b);
}

// Example usage: echo "6 10" | ./a.out
int main()
{
    int a, b;
    printf("a := ");
    scanf("%d", &a);
    printf("b := ");
    scanf("%d", &b);
    printf("gcd(%d, %d) = %d\n", a, b, gcd(a, b));
    printf("lcm(%d, %d) = %d\n", a, b, lcm(a, b));
}
